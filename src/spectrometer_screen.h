/*
    This is a header file for "spectrometer_screen.cpp"
    
    Copyright (c) 2014 Vladimir Smolyar <v_2e@ukr.net>

    This file is a part of QRadLab program.

    QRadLab is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SPECTROMETER_SCREEN_H
#define SPECTROMETER_SCREEN_H

#include <QWidget>
#include <QFrame>
#include <QResizeEvent>


//class QAction;
//class PaintAreaClass: public QWidget
class SpectrometerScreen: public QFrame
{
  Q_OBJECT
  
  public:
    SpectrometerScreen(QWidget *parent, int channels);
    ~SpectrometerScreen();
    void DrawSpectrum(int numChannels, float *spectrum);
    void drawSpectrumEdge(int numChannels);
    bool getLogScale() {return logScale;};
    void setLogScale(bool newLogScale) {logScale = newLogScale;};
    int getUpperLimit() {return upperLimit;};
    void setUpperLimit(int newUpperLimit) {upperLimit = newUpperLimit;};
    
    QImage *Image;
    QPainter *Painter;

  public slots:
    void Clear(); //TODO: is it really needed? or should it be a slot?

  protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent * event);
    
  private:
    void DrawGrid();
    
    int numChannels;
    int upperLimit;
    bool logScale;
    
};


#endif
