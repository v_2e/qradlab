#ifndef ABSORBER_H
#define ABSORBER_H
#include "unit.h"

class Absorber: public Unit
{
    Q_OBJECT
public:
    Absorber(QWidget *parent);

    double size;
public slots:
    void showProperties();
    void closeProperties();
};

#endif // ABSORBER_H
