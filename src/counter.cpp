#include <QWidget>
#include <QLCDNumber>
#include <QLabel>
#include <QGridLayout>

#include "instrument.h"
#include "counter.h"

Counter::Counter(QWidget *parent):
    Instrument(parent),
    totalParticles(0),
    particlesPerSecond(0)
{
    this->type = Instrument::COUNTER;

    displayPPS = new QLCDNumber(panel);
    displayPPS->setDigitCount(6);
    displayPPS->setPalette(Qt::gray);
    connect(displayPPS, SIGNAL(overflow()), this, SLOT(displayPPSOverflow()));

    displayTotP = new QLCDNumber(panel);
    displayTotP->setDigitCount(6);
    displayTotP->setPalette(Qt::gray);
    connect(displayTotP, SIGNAL(overflow()), this, SLOT(displayTotPOverflow()));
    
    QLabel *labelPPS = new QLabel(panel);
    labelPPS->setText(tr("Counts/sec"));
    labelPPS->setMaximumWidth(80); // TODO: size should not be hardcoded
    
    QLabel *labelTotP = new QLabel(panel);
    labelTotP->setText(tr("Total counts"));
    labelTotP->setMaximumWidth(80); // TODO: size should not be hardcoded

    QGridLayout *layout = new QGridLayout(panel);
    layout->addWidget(displayPPS, 0, 0);
    layout->addWidget(labelPPS, 0, 1);
    layout->addWidget(displayTotP, 1, 0);
    layout->addWidget(labelTotP, 1, 1);
    
    //TODO: adaptive size
    //layout->setSizeConstraint(QLayout::SetFixedSize);
    layout->setSizeConstraint(QLayout::SetMinimumSize);
    //layout->setSizeConstraint(QLayout::SetMinAndMaxSize);

     setIcon(QString(":/icons/counter-icon.svg"),QSize(150,120));
}

Counter::~Counter()
{
//    qDebug("Counter destructor \n");

}

void Counter::refresh()
{
    // If the number is back to normal, turn off the RED highlighting
    if (!this->displayPPS->checkOverflow(particlesPerSecond))
        this->displayPPS->setPalette(Qt::gray);
    this->displayPPS->display(particlesPerSecond);
    
    if (!this->displayTotP->checkOverflow(totalParticles))
        this->displayTotP->setPalette(Qt::gray);
    this->displayTotP->display(totalParticles);
}

void Counter::showProperties()
{
    properties->show();
}

void Counter::closeProperties()
{
}

void Counter::addParticles(int newParticles)
{
    particlesPerSecond = newParticles;
    totalParticles += newParticles;
}



void Counter::displayTotPOverflow()
{
    displayTotP->setPalette(Qt::red);
}

void Counter::displayPPSOverflow()
{
    displayPPS->setPalette(Qt::red);
}
