#ifndef BACKGROUNDSETTINGS_H
#define BACKGROUNDSETTINGS_H

#include <QDialog>
#include <QColorDialog>


namespace Ui {
class BackgroundSettings;
}

class BackgroundSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit BackgroundSettings(QWidget *parent = 0);
    ~BackgroundSettings();
    void build();
    void makeConnections();

    QColorDialog *paletteColor;

    struct settings{
        QColor backgroundColor,gridColor;
        int gridWidth,dotsWidth;
        int gridStep;
        bool grid,dots;

    };
    struct settings currentSettings;
    struct settings finalSettings;

signals:
    void applied();
    void grid_set();
    void dots_set();



private slots:
    void change_bgColor();
    void change_gridColor();
    void on_cancelButton_clicked();
    void on_okButton_clicked();
    void on_applyButton_clicked();
    void on_gridWidth_slider_valueChanged(int value);
    void on_gridOption_clicked();
    void on_dotsOption_clicked();
    void on_widthSpinBox_valueChanged(int arg1);

    void on_gridStep_slider_valueChanged(int value);

    void on_stepSpinBox_valueChanged(int arg1);

private:
    Ui::BackgroundSettings *ui;
};

#endif // BACKGROUNDSETTINGS_H
