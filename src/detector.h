#ifndef DETECTOR_H
#define DETECTOR_H
#include "unit.h"
#include "connecter.h"

class Detector : public Unit
{
    Q_OBJECT
public:
    Detector(QWidget *parent);
    ~Detector();
    void paintEvent(QPaintEvent *paint);
    Connecter *connecter;

    double size;

public slots:
    void showProperties();
    void closeProperties();

};

#endif // DETECTOR_H
