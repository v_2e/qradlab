#include "source.h"
#include "unit.h"
#include "isotope.h"
#include "isotopesunduq.h"

#include <QPainter>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <math.h>
#include <QtDebug>

Source::Source(QWidget *parent, IsotopeSunduq *currentSunduq):
    Unit(parent),
    activity(0),
    energy(0),
    m_mass(0),
    isotope(0)
   // energy((10*(float)rand())/RAND_MAX) // TODO: isotope energies
{
    sunduq = currentSunduq;
}

Source::~Source()
{

}

void Source::paintEvent(QPaintEvent *paint)
{
        QPainter p(this);
        p.setRenderHint(QPainter::Antialiasing);
        QBrush b(color);
        if(this->hasFocus()){
         QPen pen;
         pen.setWidth(2);
         pen.setColor(Qt::red);
         p.setPen(pen);
        }
        p.setBrush(b);
        p.drawEllipse(2,2,97,97);


        p.setPen(QPen(Qt::yellow));
        p.setBrush(QBrush(Qt::black));
        p.drawPie(5,5,90,90, 0*16, 60*16);
        p.drawPie(5,5,90,90, 120*16, 60*16);
        p.drawPie(5,5,90,90, 240*16, 60*16);
        p.setBrush(QBrush(Qt::yellow));
        p.drawEllipse(35,35,30,30);
        p.setBrush(QBrush(Qt::black));
        p.drawEllipse(40,40,20,20);
}

float Source::mass()
{
    return m_mass;  // returns mass measured in a.m.u.
}

void Source::setMass(float m)
{
    m_mass = m/1.66e-24;  // convert mass [g] to mass [a.m.u]
    if(isotope != 0)
        activity = log(2)*m_mass/(isotope->mass()*isotope->halflife());
}

//float Source::activity()
//{
//    return m_activity;
//}

void Source::setActivity(float activity)
{
    this->activity = activity; // temporary! member activity must be private
                               // todo: implement float activity() function instead of float activity member;
    m_activity = activity;
}

void Source::showProperties()
{
    properties = new QWidget();
    properties->setWindowTitle(QString("Source %1 properties").arg(id));
    properties->setMinimumWidth(250);

    QGridLayout *layout = new QGridLayout;

     QLabel *isotopeLabel = new QLabel(properties);
      isotopeLabel->setText(QString("Isotope:"));

     isotopesList = new QComboBox(properties);
      sunduq->getIsotopesList(isotopesList);

      QLabel *massLabel = new QLabel(properties);
       massLabel->setText(QString("Mass [grams]:"));

     massEdit = new QDoubleSpinBox(properties);
      //  massEdit->setText("0.0");
       massEdit->setDecimals(3);
       massEdit->setMaximum(9999.999);
       massEdit->setValue(m_mass*1.66e-24);


//     QLabel *activityLabel = new QLabel(properties);
//        activityLabel->setText(QString("Mass [grams]:"));

//      activityEdit = new QDoubleSpinBox(properties);
//       //  massEdit->setText("0.0");
//        activityEdit->setDecimals(3);
//        activityEdit->setMaximum(9999.999);
//        activityEdit->setValue(m_mass*1.66e-24);

    QPushButton *applyButton = new QPushButton(properties);
     applyButton->setText("Apply");
     connect(applyButton,SIGNAL(clicked()),this,SLOT(closeProperties()));




         // Setup layout //
    layout->addWidget(isotopeLabel,0,0);
    layout->addWidget(isotopesList,0,1);
    layout->addWidget(massLabel,1,0);
    layout->addWidget(massEdit,1,1);
    layout->addWidget(applyButton,2,0,2,2);
    properties->setLayout(layout);










    if(isotope != 0)
       isotopesList->setCurrentIndex(isotope->notation);



    properties->show();
}

void Source::closeProperties()
{
    setIsotope(Isotope::Notation(isotopesList->itemData(isotopesList->currentIndex()).toInt()));
    setMass(massEdit->value());  // mass [g].
    properties->close();
    delete isotopesList;
}

void Source::setIsotope(Isotope::Notation newIsotope)
{
   isotope = sunduq->getIsotope(newIsotope);
}
