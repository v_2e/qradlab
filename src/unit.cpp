#include "unit.h"
#include <QMouseEvent>
#include <QPainter>
#include <QMenu>
#include <QAction>
#include <QMessageBox>
#include <qmath.h>
#include <QDialog>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QPaintEvent>
#include <QTimer>
#include <QSvgRenderer>

Unit::Unit(QWidget *parent) :
    QWidget(parent)
{

    this->setMouseTracking(true);
    this->setCursor(Qt::OpenHandCursor);
    this->locked = false;
    this->sticked = true;
    stickingStep = 100;
    mousePressed = false;
    // Create the context menu of the Unit;
    contextMenu = new QMenu(this);
    // Next commands adds different actions to the context menu;
      actionLock = new QAction(tr("Lock"),this);
        contextMenu->addAction(actionLock);
        connect(actionLock,SIGNAL(triggered()),this,SLOT(setLock()));

      actionStick = new QAction(tr("Stick"),this);
        contextMenu->addAction(actionStick);
        connect(actionStick,SIGNAL(triggered()),this,SLOT(setStick()));


      actionRemove = new QAction(tr("Remove"),this);
        contextMenu->addAction(actionRemove);
        connect(actionRemove,SIGNAL(triggered()),this,SLOT(removing()));

      actionProperties = new QAction(tr("Properties"),this);
        contextMenu->addAction(actionProperties);
        connect(actionProperties,SIGNAL(triggered()),this,SLOT(showProperties()));

        properties = new QWidget();

        controls.left = false;
        controls.right = false;
        controls.up = false;
        controls.down = false;

        movingTimer = new QTimer(this);
        connect(movingTimer,SIGNAL(timeout()),this,SLOT(moveByKeys()));
}

Unit::~Unit()
{

    id--;

    //qDebug("Unit destructor");
    
    // !!! BUG !!!
    // Qt application segfaults if the widget had the cursor absent in
    // the system cursor set before deletion.
    // Namely, if the cursor had been set to Qt::OpenHandCursor
    // which is absent in the X11 cursor shapes set (cursor theme?)
    // the application segfaults on the widget deletion.
    // Setting the cursor to Qt::ArrowCursor (which is the system
    // default cursor shape and thus is present in the X11 theme)
    // just before the widget deletion, "fixes" the problem.
    
    //this->unsetCursor();
    this->setCursor(Qt::ArrowCursor);
    
    delete actionRemove;
    delete actionStick;
    delete actionLock;
    delete actionProperties;
    delete contextMenu;
    delete properties;
}

void Unit::mousePressEvent(QMouseEvent *mouse)
{
    this->raise();
    this->setFocus();
    mousePressed = true;
    pressed_x = mouse->x();
    pressed_y = mouse->y();
    this->setCursor(Qt::OpenHandCursor);
    //this->setCursor(Qt::ClosedHandCursor); // Causes SEGFAULT on unit deletion
}

void Unit::mouseReleaseEvent(QMouseEvent *mouse)
{
    mousePressed = false;
    // If click implemented by right mouse button, then emit signal rightClicked();
    if(mouse->button() == Qt::RightButton){
         if(this->isLocked() == true){
            actionLock->setText(QObject::tr("Unlock"));
         }else{
            actionLock->setText(QObject::tr("Lock"));
         }

         if(this->isSticked() == true){
            actionStick->setText(QObject::tr("Unstick"));
         }else{
            actionStick->setText(QObject::tr("Stick"));
         }


         contextMenu->exec(mouse->globalPos());
    }
    //this->setCursor(Qt::OpenHandCursor); // Causes SEGFAULT on unit deletion
}

void Unit::keyPressEvent(QKeyEvent *key)
{


    if(key->key() == Qt::Key_Left)
        controls.left = true;
    if(key->key() == Qt::Key_Right)
        controls.right = true;
    if(key->key() == Qt::Key_Up)
        controls.up = true;
    if(key->key() == Qt::Key_Down)
        controls.down = true;

    movingTimer->start(10);
    if(key->key() == Qt::Key_Delete){
        removing();
    }

}

void Unit::keyReleaseEvent(QKeyEvent *keyr)
{
    if(keyr->key() == Qt::Key_Left)
        controls.left = false;
    if(keyr->key() == Qt::Key_Right)
        controls.right = false;
    if(keyr->key() == Qt::Key_Up)
        controls.up = false;
    if(keyr->key() == Qt::Key_Down)
        controls.down = false;
    if(!(controls.up || controls.down || controls.left || controls.right))
      movingTimer->stop();
}

void Unit::moveByKeys()
{


       if(!isLocked()){
       int step = 1;
        if(isSticked()) step = stickingStep;

        if(controls.left)
            this->move(this->x() - step,this->y());
        if(controls.right)
            this->move(this->x() + step,this->y());
        if(controls.up)
            this->move(this->x(),this->y() - step);
        if(controls.down)
            this->move(this->x(),this->y() + step);


        if(this->x()<0)
         this->move(0,this->y());

        if(this->x()>parentWidget()->width()-this->width())
         this->move(parentWidget()->width()-this->width(),this->y());

        if(this->y()<0)
         this->move(this->x(),0);

        if(this->y()>parentWidget()->height()-this->height())
         this->move(this->x(),parentWidget()->height()-this->height());

       calculateRelativePos();

         if(isSticked()){
          this->move(this->x()-this->x()%stickingStep,this->y()-this->y()%stickingStep);
         }


        emit moved(id);
       }
}

void Unit::mouseMoveEvent(QMouseEvent *mouse)
{
    if(mousePressed == true){
    if(!isLocked()){
        
    emit tryMove(QPoint(this->x() - pressed_x + mouse->x(),this->y() - pressed_y + mouse->y()));

    if(this->x()<0)
     this->move(0,this->y());

    if(this->x()>parentWidget()->width()-this->width())
     this->move(parentWidget()->width()-this->width(),this->y());

    if(this->y()<0)
     this->move(this->x(),0);

    if(this->y()>parentWidget()->height()-this->height())
     this->move(this->x(),parentWidget()->height()-this->height());

   calculateRelativePos();

     if(isSticked()){
      emit tryMove(QPoint(this->x()-this->x()%stickingStep,this->y()-this->y()%stickingStep));
     }

     emit replaced();

   }
    }
    emit moved(id);

}

void Unit::paintEvent(QPaintEvent *paint)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.drawImage(0,0,icon);
}

bool Unit::isLocked()
{
    return locked;
}

bool Unit::isSticked()
{
    return sticked;
}

void Unit::setIcon(QString path, QSize iconSize)
{
    QSvgRenderer renderer(path);

    // Prepare a QImage with desired characteritisc
    QImage image(iconSize, QImage::Format_ARGB32);
    image.fill(80000000);  // partly transparent background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    icon = image;
}

double Unit::x_cm(double scaleFactor)
{
  return this->x()*scaleFactor;
}

double Unit::y_cm(double scaleFactor)
{
    return this->y()*scaleFactor;
}


void Unit::keepPosInWorkspace()
{
    if(!isLocked()){

             this->move(int(relative_x*parentWidget()->width())-this->width(),
                        int(relative_y*parentWidget()->height())-this->height());

    if(this->x() < 0)
     this->move(0,this->y());

    if(this->x() > parentWidget()->width()-this->width())
     this->move(parentWidget()->width()-this->width(),this->y());

    if(this->y() < 0)
     this->move(this->x(),0);

    if(this->y() > parentWidget()->height()-this->height())
     this->move(this->x(),parentWidget()->height()-this->height());



   }else{
    calculateRelativePos();
   }
}

void Unit::setLock()
{

    calculateRelativePos();
    if(locked == true){
        locked = false;
    }else{
        locked = true;
    }
}

void Unit::setStick()
{
    if(sticked == true){
        sticked = false;
    }else{
        sticked = true;
        this->move(this->x()-this->x()%stickingStep,this->y()-this->y()%stickingStep);
    }
    calculateRelativePos();
}

void Unit::removing()
{
    QMessageBox msgBox;
        msgBox.setText("Are you sure?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
      
     if(msgBox.exec() == QMessageBox::Yes)
        emit removeSignal(this->id);
}

void Unit::calculateRelativePos()
{
    relative_x = double(this->x()+this->width())/parentWidget()->width();
    relative_y = double(this->y()+this->height())/parentWidget()->height();
}
