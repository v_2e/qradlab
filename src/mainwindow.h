#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "ui_mainwindow.h"
#include "backgroundsettings.h"
#include "workspace.h"
#include "ruler.h"
#include "domschemedata.h"
#include "wiringboard.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void resizeEvent(QResizeEvent *resize);
   // WiringBoard *board;

public slots:
    void addDockInstrument(QWidget *instrument, QString);

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;
    void build();
    void makeConnections();
    int rulerSize;

    QMenu *menuMenu;
    QMenu *menuSettings;
    QMenu *menuSimulation;

    QAction *actionOpenFile;
    QAction *actionSave;
    QAction *actionSaveAs;
    QAction *actionExit;
    QAction *actionOpenSettings;
    QAction *actionSimulationStart;
    QAction *actionSimulationStop;
    QWidget *centralArea;
    Workspace *workspace;
    Ruler *horizontalRuler,*verticalRuler;
    DomSchemeData* domSchemeData;

signals:




private slots:
    void openProject();
    void saveProject();
    void saveProjectAs();
    void exitLAB();
    void settingsShow();
    void refresh();
    void on_addDetectorButton_clicked();
    void on_addSourceButton_clicked();
    void on_addAbsorberButton_clicked();
    void on_addInstrumentButton_clicked();
    void on_addSpectrometerButton_clicked();
    void startSimulation();
    void stopSimulation();
    void setWindIcon(QString path);

};

#endif // MAINWINDOW_H
