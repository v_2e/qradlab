#include "tooliconwidget.h"
#include <QWidget>
#include <QSvgRenderer>
#include <QPainter>

ToolIconWidget::ToolIconWidget(QWidget *parent): QWidget(parent)
{
}

void ToolIconWidget::paintEvent(QPaintEvent *pe)
{
    QPainter p(this);
    p.drawImage(0,0,icon);
    p.drawText(QRect(0,icon.height(),width(),height() - icon.height()),text);
}

void ToolIconWidget::setIcon(QString path, QSize iconSize)
{
    QSvgRenderer renderer(path);

    // Prepare a QImage with desired characteritisc
    QImage image(iconSize, QImage::Format_ARGB32);
    image.fill(80000000);  // partly transparent background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    icon = image;
}

void ToolIconWidget::setText(QString text_)
{
    text = text_;
}
