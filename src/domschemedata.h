#ifndef DOMSCHEMEDATA_H
#define DOMSCHEMEDATA_H

#include<QDomDocument>
#include<QDomElement>
#include<QFile>

#include<workspace.h>

class DomSchemeData : public QDomDocument
{
    Workspace* workspace;

    QDomNode scheme;
      QDomElement tagAbsorbers;
      QDomElement tagDetectors;
      QDomElement tagInstruments;
      QDomElement tagSources;

public:
    DomSchemeData(Workspace* workspace);

    QFile fileXML; //Scheme file

    void openSchemeFile(QString fileName);
    void saveSchemeToFile(QString fileName);
};

#endif // DOMSCHEMEDATA_H
