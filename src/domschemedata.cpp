#include "domschemedata.h"

#include<QWidget>
#include<QTextStream>
#include<QFileDialog>

#include <tagnames.h>

using namespace TagNames;

//TODO
//rename scheme - ???
DomSchemeData::DomSchemeData(Workspace* workspace)
{
    this->workspace = workspace;

    scheme = this->createElement(TAG_ROOT);
      tagAbsorbers   = this->createElement(TAG_ABSORBERS);
      tagDetectors   = this->createElement(TAG_DETECTORS);
      tagInstruments = this->createElement(TAG_INSTRUMENTS);
      tagSources     = this->createElement(TAG_SOURCES);

    this->appendChild(scheme);
      scheme.appendChild(tagAbsorbers);
      scheme.appendChild(tagDetectors);
      scheme.appendChild(tagInstruments);
      scheme.appendChild(tagSources);
}

void DomSchemeData::openSchemeFile(QString fileName)
{
    fileXML.setFileName(fileName);
    fileXML.open( QFile::ReadOnly );

    if (!this->setContent(&fileXML)){
        qWarning("Error opening file.");
        return;
    }

    fileXML.close();


    //Delete old units
    for(int j=0 ; j < workspace->units.detectors.size() ; j++){
        delete workspace->units.detectors.at(j);
    }

    for(int j=0 ; j < workspace->units.absorbers.size() ; j++){
        delete workspace->units.absorbers.at(j);
    }

    for(int j=0 ; j < workspace->units.instruments.size() ; j++){
        delete workspace->units.instruments.at(j);
    }

    for(int j=0 ; j < workspace->units.sources.size() ; j++){
        delete workspace->units.sources.at(j);
    }

    //Clear the lists
    workspace->units.absorbers.clear();
    workspace->units.detectors.clear();
    workspace->units.instruments.clear();
    workspace->units.sources.clear();

    //workspace = new Workspace(this);
    //this->setCentralWidget(workspace);

    scheme = this->firstChild();
      tagDetectors   = scheme.firstChildElement(TAG_DETECTORS);
      tagAbsorbers   = scheme.firstChildElement(TAG_ABSORBERS);
      tagInstruments = scheme.firstChildElement(TAG_INSTRUMENTS);
      tagSources     = scheme.firstChildElement(TAG_SOURCES);


    //Constructing units
    for (int i = 0; i < tagDetectors.childNodes().size(); ++i) {
        workspace->createDetector();

        QDomNode nodeDetector = tagDetectors.childNodes().at(i);
        QDomNode attrX = nodeDetector.attributes().item(0);
        QDomNode attrY = nodeDetector.attributes().item(1);

        //TODO
        //check if the items are correct INTs
        workspace->units.detectors.at(i)->setGeometry(attrX.nodeValue().toInt(),
                                                      attrY.nodeValue().toInt(),
                                                      100,
                                                      100);
    }

    for (int i = 0; i < tagAbsorbers.childNodes().size(); ++i) {
        workspace->createAbsorber();

        QDomNode nodeAbsorber = tagAbsorbers.childNodes().at(i);
        QDomNode attrX = nodeAbsorber.attributes().item(0);
        QDomNode attrY = nodeAbsorber.attributes().item(1);

        //TODO
        //check if the items are correct INTs
        workspace->units.absorbers.at(i)->setGeometry(attrX.nodeValue().toInt(),
                                                      attrY.nodeValue().toInt(),
                                                      100,
                                                      100);
    }

    for (int i = 0; i < tagInstruments.childNodes().size(); ++i) {
        workspace->createCounter();

        QDomNode nodeInstrument = tagInstruments.childNodes().at(i);
        QDomNode attrX = nodeInstrument.attributes().item(0);
        QDomNode attrY = nodeInstrument.attributes().item(1);

        //TODO
        //check if the items are correct INTs
        workspace->units.instruments.at(i)->setGeometry(attrX.nodeValue().toInt(),
                                                        attrY.nodeValue().toInt(),
                                                        100,
                                                        100);
    }

    for (int i = 0; i < tagSources.childNodes().size(); ++i) {
        workspace->createSource(); //pornography

        QDomNode nodeSource = tagSources.childNodes().at(i);
        QDomNode attrX = nodeSource.attributes().item(0);
        QDomNode attrY = nodeSource.attributes().item(1);

        //TODO
        //check if the items are correct INTs
        workspace->units.sources.at(i)->setGeometry(attrX.nodeValue().toInt(),
                                                    attrY.nodeValue().toInt(),
                                                    100,
                                                    100);
    }
}

void DomSchemeData::saveSchemeToFile(QString fileName)
{
    fileXML.setFileName(fileName);

    //Clear the scheme tree
    scheme.firstChildElement(TAG_ABSORBERS).clear();
    scheme.firstChildElement(TAG_DETECTORS).clear();
    scheme.firstChildElement(TAG_INSTRUMENTS).clear();
    scheme.firstChildElement(TAG_SOURCES).clear();

    QDomElement helperElem;

    //Constructing the scheme tree from units
    for (int i = 0; i < workspace->units.absorbers.size(); ++i) {
        helperElem = this->createElement(TAG_ABSORBER);
        helperElem.setAttribute(ATTR_X, workspace->units.absorbers.at(i)->pos().x());
        helperElem.setAttribute(ATTR_Y, workspace->units.absorbers.at(i)->pos().y());

        tagAbsorbers.appendChild(helperElem);
    }

    for (int i = 0; i < workspace->units.detectors.size(); ++i) {
        helperElem = this->createElement(TAG_DETECTOR);
        helperElem.setAttribute(ATTR_X, workspace->units.detectors.at(i)->pos().x());
        helperElem.setAttribute(ATTR_Y, workspace->units.detectors.at(i)->pos().y());

        tagDetectors.appendChild(helperElem);
    }

    for (int i = 0; i < workspace->units.instruments.size(); ++i) {
        helperElem = this->createElement(TAG_INSTRUMENT);
        helperElem.setAttribute(ATTR_X, workspace->units.instruments.at(i)->pos().x());
        helperElem.setAttribute(ATTR_Y, workspace->units.instruments.at(i)->pos().y());

        tagInstruments.appendChild(helperElem);
    }

    for (int i = 0; i < workspace->units.sources.size(); ++i) {
        helperElem = this->createElement(TAG_SOURCE);
        helperElem.setAttribute(ATTR_X, workspace->units.sources.at(i)->pos().x());
        helperElem.setAttribute(ATTR_Y, workspace->units.sources.at(i)->pos().y());

        tagSources.appendChild(helperElem);
    }

    //Write scheme tree to file
    if (fileXML.open( QFile::WriteOnly | QFile::Truncate)){
        QTextStream textStream(&fileXML);
        this->save(textStream, 0);
        fileXML.close();
    }
    else
        qWarning("Error writing to file.");
}
