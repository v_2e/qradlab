/*
    This is a header file for "spectrometer.cpp"
    
    Copyright (c) 2014 Vladimir Smolyar <v_2e@ukr.net>

    This file is a part of QRadLab program.

    QRadLab is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPECTROMETER_H
#define SPECTROMETER_H

#include <QLCDNumber>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QDial>

#include "instrument.h"
#include "spectrometer_screen.h"

class Spectrometer: public Instrument
{
    Q_OBJECT
public:
    Spectrometer(QWidget *parent); //TODO: pass number of channels
    ~Spectrometer();

    QLCDNumber *display;
    QLabel *label;

    int numChannels;
    
    void addSpectrum(int channels, float* newSpectrum);

public slots:
    //void panelShow();
    void refresh();

private:
    void checkNumChannels();
    
    //unsigned long int *Spectrum;
    float *Spectrum;
    SpectrometerScreen *Screen;
    bool mousePressed;
    
    QCheckBox *integralCheck;
    QCheckBox *logCheck;
    QPushButton *clearButton;
    QDial *limitDial, *channelsDial;
    

private slots:
    void numChannelsChanged(int newNumChannels);
    void mousePressedOn();
    void mousePressedOff();
    void setLogScale(int state);
    void setUpperLimit(int limit);
    void showProperties();
    void closeProperties();

};

#endif // SPECTROMETER_H
