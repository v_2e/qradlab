
QT       += core gui xml
QT       += svg

TARGET = QRadLab
TEMPLATE = app

DESTDIR = ../build
MOC_DIR = ../build
UI_HEADERS_DIR = ../build
UI_SOURCES_DIR = ../build
OBJECTS_DIR = ../build




SOURCES += main.cpp\
        mainwindow.cpp \
    unit.cpp \
    detector.cpp \
    backgroundsettings.cpp \
    workspace.cpp \
    absorber.cpp \
    source.cpp \
    instrument.cpp \
    domschemedata.cpp \
    counter.cpp \
    spectrometer.cpp \
    spectrometer_screen.cpp \
    ruler.cpp \
    isotope.cpp \
    connecter.cpp \
    wiringboard.cpp \
    isotopesunduq.cpp \
    tooliconwidget.cpp

HEADERS  += mainwindow.h \
    unit.h \
    detector.h \
    backgroundsettings.h \
    workspace.h \
    absorber.h \
    source.h \
    instrument.h \
    domschemedata.h \
    tagnames.h \
    counter.h \
    spectrometer.h \
    spectrometer_screen.h \
    ruler.h \
    isotope.h \
    connecter.h \
    wiringboard.h \
    isotopesunduq.h \
    tooliconwidget.h
    

FORMS    += mainwindow.ui \
    backgroundsettings.ui

RESOURCES += \
    res.qrc
