#include "backgroundsettings.h"
#include "ui_backgroundsettings.h"
#include <QPainter>

BackgroundSettings::BackgroundSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BackgroundSettings)
{
    // TODO: rework without .ui file
    ui->setupUi(this);
    build();
    makeConnections();
    
    ui->current_bgColor->setFlat(true);
    ui->current_bgColor->setAutoFillBackground(true);
    ui->current_gridColor->setFlat(true);
    ui->current_gridColor->setAutoFillBackground(true);
    
}

BackgroundSettings::~BackgroundSettings()
{
    delete ui;
}

void BackgroundSettings::build()
{
    // Set sizable = false, and  window title;
    this->setWindowTitle("Settings");
    this->setFixedSize(this->width(),this->height());

    paletteColor = new QColorDialog(this);
    // current_bgColor - is a widget which display current background color;
    //ui->current_bgColor->erase();
    // current_gridColor - is a widget which display current grid color;
    //ui->current_gridColor->erase();

     // currentSettings - is a structure, which combines the CURRENT VALUES of the all
     // workspace parameters in one simple structure;
     // grid(or dots) - boolean variable, which responsible for displaing the grid(or dots) on the workspace;
     // Note, that at the same time the values of this two variables can't be equal. It means that if
     // grid = true, then dots=false;
     currentSettings.grid = false;
     currentSettings.dots = true;

     currentSettings.gridWidth = 1;
     currentSettings.gridStep = 20;                // need upgrade
     currentSettings.backgroundColor = QColor(qRgb(236,240,223));
     currentSettings.gridColor =QColor(qRgb(140,140,140));
    
    // Refresh the color indicator squares
    QPalette currentPalette = ui->current_bgColor->palette();
    currentPalette.setColor(QPalette::Window, currentSettings.backgroundColor);
    //currentPalette.setColor(QPalette::Window, QColor(255,255,0));
    ui->current_bgColor->setPalette(currentPalette);
    ui->current_bgColor->setBackgroundRole(QPalette::Window);
    
    currentPalette = ui->current_gridColor->palette();
    currentPalette.setColor(QPalette::Window, currentSettings.gridColor);
    ui->current_gridColor->setPalette(currentPalette);
    ui->current_gridColor->setBackgroundRole(QPalette::Window);


     // finalSettings - is a structure, which combines the values of the all workspace parameters
     // which will returned to the workspace at closing of this widget ;
     finalSettings = currentSettings;

    // function singleColor() fills the current_bgColor widget in the chosen color,
    // in following case current_bgColor fills in finalSettings.backgroundColor;
    //ui->current_bgColor->singleColor(finalSettings.backgroundColor);
    //ui->current_gridColor->singleColor(finalSettings.gridColor);
    ui->gridOption->setChecked(finalSettings.grid);
}

void BackgroundSettings::makeConnections()
{
    connect(ui->current_bgColor,SIGNAL(clicked()),this,SLOT(change_bgColor()));
    connect(ui->current_gridColor,SIGNAL(clicked()),this,SLOT(change_gridColor()));
}


void BackgroundSettings::on_cancelButton_clicked()
{
    currentSettings = finalSettings;

    //if(currentSettings.gridColor.isValid() == true)
        //ui->current_gridColor->singleColor(currentSettings.gridColor);
    //if(currentSettings.backgroundColor.isValid() == true)
        //ui->current_bgColor->singleColor(currentSettings.backgroundColor);
    ui->gridOption->setChecked(currentSettings.grid);
    ui->dotsOption->setChecked(currentSettings.dots);
    ui->gridWidth_slider->setValue(currentSettings.gridWidth);
    ui->gridStep_slider->setValue(currentSettings.gridStep);
        this->close();
}

void BackgroundSettings::on_okButton_clicked()
{
    if(currentSettings.backgroundColor.isValid() == true)
     finalSettings.backgroundColor = currentSettings.backgroundColor;
    if(currentSettings.gridColor.isValid() == true)
     finalSettings.gridColor = currentSettings.gridColor;
    finalSettings.gridWidth = currentSettings.gridWidth;
    finalSettings.dotsWidth = currentSettings.dotsWidth;
    finalSettings.grid = currentSettings.grid;
    finalSettings.dots = currentSettings.dots;
    finalSettings.gridStep = currentSettings.gridStep;
    this->close();
    emit applied();
}

void BackgroundSettings::on_applyButton_clicked()
{
    if(currentSettings.backgroundColor.isValid() == true)
     finalSettings.backgroundColor = currentSettings.backgroundColor;
    if(currentSettings.gridColor.isValid() == true)
     finalSettings.gridColor = currentSettings.gridColor;
    finalSettings.gridWidth = currentSettings.gridWidth;
    finalSettings.dotsWidth = currentSettings.dotsWidth;
    finalSettings.grid = currentSettings.grid;
    finalSettings.dots = currentSettings.dots;
    finalSettings.gridStep = currentSettings.gridStep;
    emit applied();
}

void BackgroundSettings::on_gridWidth_slider_valueChanged(int value)
{
   currentSettings.gridWidth=value;
   ui->widthSpinBox->setValue(value);
}

void BackgroundSettings::on_gridOption_clicked()
{
    // Grid will be displayed on the workspace;
    currentSettings.grid=true;
    currentSettings.dots=false;
}

void BackgroundSettings::on_dotsOption_clicked()
{
    // Dots will be displayed on the workspace;
    currentSettings.grid=false;
    currentSettings.dots=true;
}

void BackgroundSettings::change_bgColor()
{
    // Oppens the QColorDialog to change the background color;
    QColor selectedColor = this->paletteColor->getColor(currentSettings.backgroundColor, this);
    if(selectedColor.isValid() == true)
        currentSettings.backgroundColor = selectedColor;
    // And instantly displays chosen color on current_bgColor widget;
    
    QPalette currentPalette = ui->current_bgColor->palette();
    currentPalette.setColor(QPalette::Window, currentSettings.backgroundColor);
    ui->current_bgColor->setPalette(currentPalette);
    ui->current_bgColor->setBackgroundRole(QPalette::Window);
    //ui->current_bgColor->singleColor(currentSettings.backgroundColor);
}

void BackgroundSettings::change_gridColor()
{
    // Oppens the QColorDialog to change the grid color;
    QColor selected = this->paletteColor->getColor(currentSettings.gridColor, this);
    if(selected.isValid() == true)
         currentSettings.gridColor = selected;
    // And instantly displays chosen color on current_gridColor widget;
    
    QPalette currentPalette = ui->current_gridColor->palette();
    currentPalette.setColor(QPalette::Window, currentSettings.gridColor);
    ui->current_gridColor->setPalette(currentPalette);
    ui->current_gridColor->setBackgroundRole(QPalette::Window);
    
    //ui->current_gridColor->singleColor(currentSettings.gridColor);
}

void BackgroundSettings::on_widthSpinBox_valueChanged(int arg1)
{
    currentSettings.gridWidth = arg1;
    ui->gridWidth_slider->setValue(arg1);
}

void BackgroundSettings::on_gridStep_slider_valueChanged(int value)
{
    currentSettings.gridStep=value;
    ui->stepSpinBox->setValue(value);
}

void BackgroundSettings::on_stepSpinBox_valueChanged(int arg1)
{
    currentSettings.gridStep = arg1;
    ui->gridStep_slider->setValue(arg1);
}
