#ifndef UNIT_H
#define UNIT_H

#include <QWidget>
#include <QMenu>
#include <QDockWidget>

class Unit : public QWidget
{
    Q_OBJECT
public:
    explicit Unit(QWidget *parent = 0);
    ~Unit();
    void mousePressEvent(QMouseEvent *mouse);
    void mouseReleaseEvent(QMouseEvent *mouse);
    void keyPressEvent(QKeyEvent *key);
    void keyReleaseEvent(QKeyEvent *keyr);
    virtual void mouseMoveEvent(QMouseEvent *mouse);
    virtual void paintEvent(QPaintEvent *paint);
    bool isLocked();
    bool isSticked();


    /* Only for .svg icons */
    void setIcon(QString path,QSize iconSize);

    bool sticked,mousePressed;
    bool locked;
    int pressed_x,pressed_y,stickingStep;

    // double x_cm - x coordinate of unit in [cm];
    double relative_x,relative_y;
    double scaleFactor;
    double x_cm(double scaleFactor);
    double y_cm(double scaleFactor);
    double size;

    struct Controls{
        bool left;
        bool right;
        bool up;
        bool down;
    };

    Controls controls;
    QTimer *movingTimer;

    int id; // unit identifier


    QColor color;
    QMenu *contextMenu;
    QAction *actionLock,*actionRemove,*actionStick,*actionProperties;
    QWidget *properties;   // TODO : Rename properties -> propertiesDialog


signals:
    void moved(int i);
    void clicked();
    void rightClicked();
    void removeSignal(int i);
    void replaced();
    void tryMove(QPoint toPoint);

public slots:
    void keepPosInWorkspace();
    void setLock();
    void setStick();
    void removing();
    void calculateRelativePos();
    virtual void showProperties() = 0;

    // TODO  rename closeProperties -> applyProperties
    virtual void closeProperties() = 0;
    void moveByKeys();

protected:
    QImage icon;
};

#endif // UNIT_H
