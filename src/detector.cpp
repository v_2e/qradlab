#include "detector.h"
#include "unit.h"
#include <QPainter>
#include <QWidget>
#include <QSvgRenderer>


Detector::Detector(QWidget *parent):
    Unit(parent),
    size(5.0)
{
    connect(this->actionProperties,SIGNAL(triggered()),this,SLOT(showProperties()));
    connecter = new Connecter(Connecter::OUT, this);
  //  connecter->setGeometry(65,65,15,15);
    connecter->move(120,50);
    setIcon(QString(":/icons/detector-icon.svg"),QSize(160,120));

}

Detector::~Detector()
{
    delete connecter;
}

void Detector::paintEvent(QPaintEvent *paint)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.drawImage(0,0,icon);
}

void Detector::showProperties()
{
   // properties->show();
    properties->show();
}

void Detector::closeProperties()
{
}
