#ifndef COUNTER_H
#define COUNTER_H
#include "instrument.h"
#include <QLCDNumber>
#include <QLabel>

class Counter: public Instrument
{
    Q_OBJECT
public:


    Counter(QWidget *parent);
    ~Counter();


    void addParticles(int newParticles);



public slots:
    //void panelShow();
    void refresh();
    void showProperties();
    void closeProperties();

private:
    int totalParticles, particlesPerSecond;
    QLCDNumber *displayTotP, *displayPPS;

    //QLabel *labelTotP, *labelPPS;

private slots:
    void displayTotPOverflow();
    void displayPPSOverflow();
};

#endif // COUNTER_H
