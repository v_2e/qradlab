#ifndef SOURCE_H
#define SOURCE_H
#include"unit.h"
#include <QLineEdit>
#include <isotope.h>
#include <isotopesunduq.h>
#include <QComboBox>
#include <QDoubleSpinBox>

class Source: public Unit
{

    Q_OBJECT
public:
    Source(QWidget *parent, IsotopeSunduq *currentSunduq);
    ~Source();
    void paintEvent(QPaintEvent *paint);
    float activity;
    float energy; // Must be a series of energies for each line in the future
    float mass();  // [atomic mass units]
    void setMass(float m);  // [atomic mass units]
   // float activity();  // [Becquerels]
    void setActivity(float activity);  // [Becquerels]


    void setIsotope(Isotope::Notation newIsotope);


public slots:
    void showProperties();
    void closeProperties();

private:
    Isotope *isotope;
    IsotopeSunduq *sunduq;
    QComboBox *isotopesList;
    QDoubleSpinBox *massEdit;
    QDoubleSpinBox *activityEdit;
    float m_mass;
    float m_activity;

};

#endif // SOURCE_H
