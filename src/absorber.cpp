#include "absorber.h"
#include "unit.h"

Absorber::Absorber(QWidget *parent):
    Unit(parent),
    size(5)
{
    setIcon(QString(":/icons/absorber-icon.svg"),QSize(200,200));
}

void Absorber::showProperties()
{
    properties->show();
}

void Absorber::closeProperties()
{

}
