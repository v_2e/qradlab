#ifndef CONNECTER_H
#define CONNECTER_H

#include <QWidget>
#include <QMenu>

class Connecter : public QWidget
{
    Q_OBJECT
public:
     enum TYPE {IN,OUT};

    Connecter(Connecter::TYPE type, QWidget *parent = 0);
    ~Connecter();
    void paintEvent(QPaintEvent *p);
    void mousePressEvent(QMouseEvent *mousep);
    void mouseMoveEvent(QMouseEvent *mousem);
    void mouseReleaseEvent(QMouseEvent *mouser);
    TYPE type();
    void setIcon(QString path,QSize iconSize);

signals:
    void activated();
    void released();
    void mouseMoved(QPoint);
    void stopConnection();

public slots:
    void activate();
    void cutConnections();



private:
    TYPE type_;
    QMenu *contextMenu;
    QAction *actionCut;
    QImage icon;

    
};

#endif // CONNECTER_H
