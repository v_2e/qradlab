#ifndef WIRINGBOARD_H
#define WIRINGBOARD_H

#include <QWidget>
#include <unit.h>
#include <connecter.h>


class WiringBoard : public QWidget
{
    Q_OBJECT
public:
    explicit WiringBoard(QWidget *parent = 0);
    void paintEvent(QPaintEvent *paint);
    void keyPressEvent(QKeyEvent *key);
    void mouseMoveEvent(QMouseEvent *moved);
    void connectUnits();
    void setCursorPos(QPoint pos);
    QPoint getCursorPos();
    bool waiting;
    int cursorX,cursorY;
    struct Pair{
        Connecter *a,*b;
    };
    Pair newPair;
    Connecter *connecterWaitingForConnect;
    QList<Pair> connections;
    void setForkIcon(QString path,QSize iconSize);



signals:
    
public slots:
    void wait();
    void resume();
    void connectUnits(Connecter *a,Connecter *b);
    void disconnectUnits(Connecter *a, Connecter *b);
    void connecterActivated();
    void connectionCut();
private:
    QPoint cursorPos;
    QImage forkIcon;
    int forkHalfWidth;
    int forkHalfHeight;
    
};

#endif // WIRINGBOARD_H
