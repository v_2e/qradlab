#include "ruler.h"
#include <QPainter>
#include <QImage>
#include <QMouseEvent>

Ruler::Ruler(QWidget *parent) :
    QWidget(parent),
    rulerSize(25),
    maxLenght(2048)
{
    setMouseTracking(true);
    QImage temp(QSize(rulerSize,rulerSize),QImage:: Format_RGB32);
    marking = temp;
//    mouseGrabb(125);
}

void Ruler::paintEvent(QPaintEvent *paint)
{
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);
    if(vertical == true){
       p.drawImage(0,0,marking);
         p.setCompositionMode(QPainter::CompositionMode_Multiply);
         p.drawImage(rulerSize-8,arrowPos-4,arrow);
    }else if(horizontal == true){
       p.drawImage(0,0,marking);
         p.setCompositionMode(QPainter::CompositionMode_Multiply);
         p.drawImage(arrowPos-4,rulerSize-8,arrow);
    }
}

void Ruler::mouseMoveEvent(QMouseEvent *moved)
{
    if(horizontal)
      arrowPos = moved->x();
    if(vertical)
      arrowPos = moved->y();
    update();
}

void Ruler::resizeEvent(QResizeEvent *resized)
{

}

void Ruler::setHorizontal()
{

    QImage temp(QSize(maxLenght,rulerSize),QImage:: Format_RGB32);
    QPainter p(&temp);
    QString mark;
    QFont fnt("Times", 8);
    int step = 4;

    p.setFont(fnt);
    p.eraseRect(0,0,maxLenght,rulerSize);
    p.drawText(0,rulerSize-12,mark.number(0));
    for(int i = 0; i < maxLenght/step; i++){
        if(i%10 == 0){
          p.drawLine(i*step,rulerSize-10,i*step,rulerSize);
          p.drawText(i*step-5,rulerSize-12,mark.number(i));
        }else if(i%5 ==0){
          p.drawLine(i*step,rulerSize-6,i*step,rulerSize);

        }else
         p.drawLine(i*step,rulerSize-3,i*step,rulerSize);
     }

    marking = temp;
    this->horizontal = true;
    this->vertical = false;
    drawArrow();
    this->update();
}

void Ruler::setVertical()
{
    QImage temp(QSize(rulerSize,maxLenght),QImage:: Format_RGB32);
    QPainter p(&temp);
    QString mark;
    QFont fnt("Times", 8);
    int step = 4;

    p.eraseRect(0,0,rulerSize,maxLenght);
    p.setFont(fnt);

    for(int i = 0; i < maxLenght/step; i++){
        if(i%10 == 0){
         p.drawLine(rulerSize-10,i*step,rulerSize,i*step);
         p.drawText(3,i*step,mark.number(i));
        }
        else if(i%5 ==0){
         p.drawLine(rulerSize-6,i*step,rulerSize,i*step);
        }
        else{
         p.drawLine(rulerSize-3,i*step,rulerSize,i*step);
        }
    }
    marking = temp;
    this->vertical = true;
    this->horizontal = false;
    drawArrow();
    this->update();
}

void Ruler::drawArrow()
{
    QImage arw(QSize(8,8),QImage::Format_ARGB32);
    QPainter p(&arw);

    p.eraseRect(0,0,7,7);
    QBrush b(Qt::black);
    p.setBrush(b);

    if(horizontal){

    QPoint points[3]={
        QPoint(0,0),
        QPoint(7,0),
        QPoint(4,7),
    };

    p.drawConvexPolygon(points, 3);

    }else{
        QPoint points[3]={
            QPoint(0,0),
            QPoint(0,7),
            QPoint(7,4),
        };
   p.drawConvexPolygon(points, 3);

    }



    arrow = arw;
}

void Ruler::mouseGrabb(int pos)
{
    arrowPos = pos;
    update();
}


