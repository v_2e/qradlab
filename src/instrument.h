 #ifndef INSTRUMENT_H
#define INSTRUMENT_H
#include "unit.h"
#include <QDialog>
#include "connecter.h"
#include "detector.h"

class Instrument: public Unit
{
    Q_OBJECT
public:
    Instrument(QWidget *parent);
    ~Instrument();
    static const int COUNTER=1;
    static const int SPECTROMETER=2;

    void mouseDoubleClickEvent(QMouseEvent *doubleClick);
    QWidget *panel;
    Connecter *connecter;
    int type;

    void setDetector(Detector *detector_);
    Detector * getDetector() {return detector;};
    

signals:
    void dockMe(QWidget *, QString);

public slots:
    void panelShow();
    virtual void refresh() = 0;


private:
    Detector *detector;


};

#endif // INSTRUMENT_H
