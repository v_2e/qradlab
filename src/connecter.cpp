#include "connecter.h"
#include <QPainter>
#include <QMouseEvent>
#include <QMenu>
#include <QDebug>
#include <QSvgRenderer>
#include <QPainter>


/*  TODO: each of instruments can use only one detector (yet), but old connections still
 *        exist after you connect new detector to an instrument */

Connecter::Connecter(Connecter::TYPE type, QWidget *parent) :
    QWidget(parent)
{
    type_ = type;
    this->setFixedSize(35,20);
    this->setCursor(Qt::CrossCursor);

    contextMenu = new QMenu(this);
      actionCut = new QAction(tr("Cut Connections"),this);
        contextMenu->addAction(actionCut);
        connect(actionCut,SIGNAL(triggered()),this,SLOT(cutConnections()));
        if(type == Connecter::IN){
            setIcon(QString(":/icons/connecter_in_icon.svg"),size());
        }else{
           setIcon(QString(":/icons/connecter_out_icon.svg"),size());
        }
}

Connecter::~Connecter()
{
    emit stopConnection();
}

void Connecter::paintEvent(QPaintEvent *p)
{
    QPainter pntr(this);
 //   pntr.setRenderHint(QPainter::Antialiasing);
    pntr.drawImage(0,0,icon);
}

void Connecter::mousePressEvent(QMouseEvent *mousep)
{
    if(mousep->button() == Qt::RightButton){

    }else{
      emit activated();
    }

}

void Connecter::mouseMoveEvent(QMouseEvent *mousem)
{
    emit mouseMoved(mousem->pos());
}

void Connecter::mouseReleaseEvent(QMouseEvent *mouser)
{
    if(mouser->button() == Qt::RightButton){
        contextMenu->exec(mouser->globalPos());
    }else{
       emit released();
    }
}

Connecter::TYPE Connecter::type()
{
    return type_;
}

void Connecter::activate()
{
    emit activated();
}

void Connecter::cutConnections()
{
    emit stopConnection();
}

void Connecter::setIcon(QString path, QSize iconSize)
{
    QSvgRenderer renderer(path);

    // Prepare a QImage with desired characteritisc
    QImage image(iconSize, QImage::Format_ARGB32);
    image.fill(80000000);  // partly transparent red-ish background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    icon = image;
}
