#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <unistd.h>

#include <QWidget>
#include <QWidget>
#include <QPainter>
#include <QList>
#include <QMenu>
#include <QTimer>

#include "backgroundsettings.h"
#include "unit.h"
#include "detector.h"
#include "counter.h"
#include "spectrometer.h"
#include "source.h"
#include "absorber.h"
#include "instrument.h"
#include <wiringboard.h>
#include "isotopesunduq.h"

class Workspace : public QWidget
{
    Q_OBJECT

    struct Units{
        QList<Detector*>   detectors;
        QList<Instrument*>    instruments;
        QList<Source*>     sources;
        QList<Absorber*>   absorbers;
    };


public:
    Workspace(QWidget *parent);
    ~Workspace();
    void paintEvent(QPaintEvent *paint);
    void resizeEvent(QResizeEvent *resize);
    void mouseReleaseEvent(QMouseEvent *mouse);
    void mouseMoveEvent(QMouseEvent *moved);
    void mousePressEvent(QMouseEvent *pressed);
    BackgroundSettings *bgsettings;
    void cursorHighlight(int x, int y);
    QWidget *mainwindow;
    QMenu *contextMenu;
    QAction *actionAllLock,*actionAllUnlock,*actionDeleteAll,*actionCursorHighlight;
    bool cursorHighlighting;
    int cursor_x,cursor_y;
    double scale;

    int unitsCounter;
    void createSource();
    void createAbsorber();
    void createDetector();
    Instrument *createCounter();
    Instrument *createSpectrometer();
    void selectUnit(Unit *unit);
    Unit *selectedUnit();

    WiringBoard *board;
    IsotopeSunduq *sunduq;
    Units units;



    void calculateTransmissionMatrix(int numSources, int numDetectors, float** TransmissionMatrix);


private:
    QTimer *WorkingTimer;
    QImage *background;
    Unit *m_selectedUnit;
    int gridStep;

signals:
    void resized();
    void cursorX(int x);
    void cursorY(int y);


public slots:
    void deleteUnit(int ID);

    void refresh();
    void applySettings();

    void allLock();
    void allUnlock();
    void deleteAll();
    void startSimulation();
    void stopSimulation();
    void lookingForConnecter(QPoint point);
    void tryConnect();

private slots:
    void backgroundFill();
    void Simulation();
    void deleteAllRequest();
    void setCursorHighlighing();
    void onUnitCursorMoved();
    void moveUnit(QPoint toPoint);

};

#endif // WORKSPACE_H
