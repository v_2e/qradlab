#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "workspace.h"
#include "ruler.h"
#include <wiringboard.h>
#include "tooliconwidget.h"

#include <QWidget>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QPainter>
#include <unistd.h>
#include <QList>


#include <math.h>

#include <QDomDocument>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QDockWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QToolBar>
#include <QSvgRenderer>

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent),
    ui(new Ui::MainWindow),
    rulerSize(25)
{
   ui->setupUi(this);
   setWindIcon(":/icons/window-icon.svg");
   build();
   makeConnections();

   domSchemeData = new DomSchemeData(workspace);
   
   this->setDockOptions(QMainWindow::AnimatedDocks | QMainWindow::AllowNestedDocks | QMainWindow::AllowTabbedDocks);
   this->setTabPosition(Qt::RightDockWidgetArea, QTabWidget::North);
   QToolBar *leftToolBar = new QToolBar;
   leftToolBar->setMovable(false);
   leftToolBar->setAllowedAreas(Qt::LeftToolBarArea);
   this->addToolBar(Qt::LeftToolBarArea,leftToolBar);





   /* Try to add spectrometer icon to the toolbar.                            */
   /*   ???????????????????????????????????????????????????????????????????   */
   ToolIconWidget *spectrometerTool = new ToolIconWidget(this);
  // spectrometerTool->resize(150,180);
   spectrometerTool->setIcon(QString(":/icons/spectrometer-icon.svg"),QSize(150,120));
   leftToolBar->addWidget(spectrometerTool);
   /*    ??????????????????????????????????????????????????????????????????   */



   leftToolBar->addWidget(ui->addSourceButton);
   leftToolBar->addWidget(ui->addDetectorButton);
   leftToolBar->addWidget(ui->addAbsorberButton);
   leftToolBar->addWidget(ui->addSpectrometerButton);
   leftToolBar->addWidget(ui->addInstrumentButton);
   leftToolBar->addWidget(ui->runButton);




}

MainWindow::~MainWindow()
{
    //  qDebug("MainWindow destructor \n");
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *resize){

}

void MainWindow::build()
{
    // Adds different Widgets to mainwindow;
    // This function called in the mainwindow constructor;

    // Following commands creates the menu on the mainwindow;

    actionOpenFile = new QAction(tr("Open file"), this);
      actionOpenFile->setStatusTip(tr("Open project file"));

    actionSave = new QAction(tr("Save project"), this);
      actionSave->setStatusTip(tr("Save project to file"));

    actionSaveAs = new QAction(tr("Save project as... "), this);
      actionSaveAs->setStatusTip(tr("Save project as ..."));

    actionExit = new QAction(tr("Exit"),this);
      actionExit->setStatusTip(tr("Exit QRadLab"));

    actionSimulationStart = new QAction(tr("Start"), this);
      actionSimulationStart->setStatusTip(tr("Start simulation"));
      actionSimulationStart->setEnabled(true);

    actionSimulationStop = new QAction(tr("Stop"), this);
      actionSimulationStop->setStatusTip(tr("Stop simulation"));
      actionSimulationStop->setEnabled(false);

    actionOpenSettings = new QAction(tr("Background"),this);
      actionOpenSettings->setStatusTip(tr("Configurate background color"));

    menuMenu = menuBar()->addMenu(tr("Menu"));
      menuMenu->addAction(actionOpenFile);
      menuMenu->addAction(actionSave);
      menuMenu->addAction(actionSaveAs);
      menuMenu->addAction(actionExit);

    menuSettings = menuBar()->addMenu(tr("Simulation"));
      menuSettings->addAction(actionSimulationStart);
      menuSettings->addAction(actionSimulationStop);


    menuSettings = menuBar()->addMenu(tr("Settings"));
      menuSettings->addAction(actionOpenSettings);

     // Creates the workspace as the centralWidget of mainwindow;

      centralArea = new QWidget(this);
      QGridLayout *workspaceLayout = new QGridLayout();

      QPushButton *metricButton = new QPushButton(this);
      metricButton->setFont(QFont("Times",8));
      metricButton->setText("mm");
      metricButton->setFixedSize(rulerSize,rulerSize);


      verticalRuler = new Ruler(this);
      verticalRuler->setVertical();
      verticalRuler->setMinimumWidth(rulerSize);
      verticalRuler->setMaximumWidth(rulerSize);

      horizontalRuler = new Ruler(this);
      horizontalRuler->setHorizontal();
      horizontalRuler->setMinimumHeight(rulerSize);
      horizontalRuler->setMaximumHeight(rulerSize);


      workspace = new Workspace(this);
      workspace->setMouseTracking(true);


//      QWidget *stateBar = new QWidget(centralArea);

//      workspaceLayout->addWidget(stateBar,0,0,0,1);
//      workspaceLayout->addWidget(metricButton,1,0);
//      workspaceLayout->addWidget(verticalRuler,2,0);
//      workspaceLayout->addWidget(horizontalRuler,1,1);
//      workspaceLayout->addWidget(workspace,2,1);




      workspaceLayout->addWidget(metricButton,0,0);
      workspaceLayout->addWidget(verticalRuler,1,0);
      workspaceLayout->addWidget(horizontalRuler,0,1);
      workspaceLayout->addWidget(workspace,1,1);

      metricButton->setGeometry(0,0,10,10);
      workspaceLayout->setContentsMargins(0,0,0,0);
      workspaceLayout->setHorizontalSpacing(1);
      workspaceLayout->setVerticalSpacing(1);





      centralArea->setLayout(workspaceLayout);
     this->setCentralWidget(centralArea);



     connect(workspace,SIGNAL(cursorX(int)),horizontalRuler,SLOT(mouseGrabb(int)));
     connect(workspace,SIGNAL(cursorY(int)),verticalRuler,SLOT(mouseGrabb(int)));


}

void MainWindow::makeConnections()
{
    // Connects the slots with the corresponding signals;
    // All connections which implemented in mainwindow.cpp will be presented here
    // to simplify constructor structure;
    // This function called in the mainwindow constructor;

    connect(this->workspace->bgsettings,SIGNAL(applied()),this,SLOT(refresh()));
    connect(actionOpenFile, SIGNAL(triggered()), this, SLOT(openProject()));
    connect(actionSave,     SIGNAL(triggered()), this, SLOT(saveProject()));
    connect(actionSaveAs,   SIGNAL(triggered()), this, SLOT(saveProjectAs()));
    connect(actionExit,SIGNAL(triggered()),this,SLOT(exitLAB()));

    connect(actionSimulationStart,SIGNAL(triggered()), this, SLOT(startSimulation()));
    connect(actionSimulationStop,SIGNAL(triggered()), this, SLOT(stopSimulation()));

    //connect(ui->runButton,SIGNAL(clicked()),this,SLOT(start_simulation()));
    connect(ui->runButton,SIGNAL(clicked()),this,SLOT(startSimulation()));

    connect(actionOpenSettings,SIGNAL(triggered()),this,SLOT(settingsShow()));
}

void MainWindow::exitLAB()
{
    // Closes the application.
    this->close();
}

void MainWindow::settingsShow()
{
    //Opens BackgroundSettings window, in which the user can customize this application.
    workspace->bgsettings->show();
}

void MainWindow::refresh()
{
    this->centralWidget()->update(); // Recommended instead of repaint() by Qt documentation
}

void MainWindow::on_addDetectorButton_clicked()
{
    //function createSomeUnit()creates a Unit on the workspace and adds it to the list of corresponding units.
    workspace->createDetector();
}

void MainWindow::on_addSourceButton_clicked()
{
    //function createSomeUnit()creates a Unit on the workspace and adds it to the list of corresponding units.
    workspace->createSource();
}

void MainWindow::on_addAbsorberButton_clicked()
{
   //function getSomeUnit()creates a Unit on the workspace and adds it to the list of corresponding units.
   workspace->createAbsorber();
}

void MainWindow::on_addInstrumentButton_clicked()
{
   //function createSomeUnit()creates a Unit on the workspace and adds it to the list of corresponding units.
   Instrument * newInstrument = workspace->createCounter();
   connect(newInstrument, SIGNAL(dockMe(QWidget*, QString)), this, SLOT(addDockInstrument(QWidget*, QString)));
}

void MainWindow::on_addSpectrometerButton_clicked()
{
   //function createSomeUnit()creates a Unit on the workspace and adds it to the list of corresponding units.
   Instrument * newInstrument = workspace->createSpectrometer();
   connect(newInstrument, SIGNAL(dockMe(QWidget*, QString)), this, SLOT(addDockInstrument(QWidget*, QString)));
}

void MainWindow::openProject()
{
    QString fileName = QFileDialog::getOpenFileName(
                    this,
                    trUtf8("Open project file"),
                    trUtf8("/home/MeDinaGentoo/"),
                    trUtf8("QRadLab files (*.qrl)")
                );

    domSchemeData->openSchemeFile(fileName);
}

void MainWindow::saveProject()
{
    //Check if fileXML.fileName() is defined (have been opened or saved to)
    QString fileName;
    if (!QString::compare(domSchemeData->fileXML.fileName(), QString(""))) {
        fileName = QFileDialog::getSaveFileName(
                        this,
                        trUtf8("Save project file"),
                        trUtf8("/home/MeDinaGentoo/"),
                        trUtf8("QRadLab files (*.qrl)")
                    );
    } else {
        fileName = domSchemeData->fileXML.fileName();
    }

    domSchemeData->saveSchemeToFile(fileName);
}

float gauss(float x, float mu, float sigma)
{
    return (1/(sigma*sqrt(2*M_PI))) * exp(-powf((x-mu),2)/(2*sigma*sigma));
}

void MainWindow::saveProjectAs()
{
    QString fileName = QFileDialog::getSaveFileName(
                    this,
                    trUtf8("Save project file"),
                    trUtf8("/home/MeDinaGentoo/"),
                    trUtf8("QRadLab files (*.qrl)")
                );

    domSchemeData->saveSchemeToFile(fileName);
}

void MainWindow::startSimulation()
{
    // TODO: maybe block something ?
    
    this->actionSimulationStop->setEnabled(true);
    this->actionSimulationStart->setEnabled(false);
    
    ui->runButton->setText(tr("STOP"));
    ui->runButton->setPalette(QPalette(
        Qt::black,             // windowText
        QColor(165, 24, 24),   // button
        QColor(249, 36, 36),   // light
        QColor(82,  12, 12),   // dark
        QColor(110, 16, 16),   // mid
        QColor(  0,  0,  0),   // text
        QColor(255,255,255),   // bright_text
        QColor(255,255,255),   // base
        QColor(165, 24, 24))); // window
    disconnect(ui->runButton,SIGNAL(clicked()));
    connect(ui->runButton,SIGNAL(clicked()),this,SLOT(stopSimulation()));
    
    workspace->startSimulation();
}

void MainWindow::stopSimulation()
{
    workspace->stopSimulation();
 
    this->actionSimulationStop->setEnabled(false);
    this->actionSimulationStart->setEnabled(true);
    
    ui->runButton->setText(tr("RUN"));
    ui->runButton->setPalette(QPalette(Qt::darkGreen));
    ui->runButton->setPalette(QPalette(
        Qt::black, QColor(43,165,24), 
        QColor(64,248,36), QColor(21,82,12),
        QColor(28,110,16), QColor(0,0,0),
        QColor(255,255,255), QColor(255,255,255),
        QColor(43,165,24)));
    disconnect(ui->runButton,SIGNAL(clicked()));
    connect(ui->runButton,SIGNAL(clicked()),this,SLOT(startSimulation()));
    
    // TODO: maybe unblock something ?
}

void MainWindow::setWindIcon(QString path)
{
    QSvgRenderer renderer(path);
    // Prepare a QImage with desired characteritisc
    QSize iconSize(300,300);
    QImage image(iconSize, QImage::Format_ARGB32);
    image.fill(80000000);  // partly transparent background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    this->setWindowIcon(QIcon(QPixmap::fromImage(image)));
}


void MainWindow::closeEvent(QCloseEvent *event)
 {
    delete this->workspace;
    event->accept();
     //if (maybeSave()) {
         //writeSettings();
         //event->accept();
     //} else {
         //event->ignore();
     //}
 }
 
void MainWindow::addDockInstrument(QWidget *instrumentPanel, QString instrumentName)
{
    if (instrumentPanel->isVisible()) return;
    
    // TODO: determine the instrument's personal name
   
    QDockWidget *dock = new QDockWidget(instrumentName, this);
    dock->setAllowedAreas(Qt::RightDockWidgetArea);
    //dock->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    //instrumentPanel->setParent(dock);
    dock->setWidget(instrumentPanel);
    //dock->setFloating(true); // TODO: set default or not? - *** NO!!!
    this->addDockWidget(Qt::RightDockWidgetArea, dock);

    // TODO: check if the docked widget is displayed correctly
    
    //dock->setFloating(false);
    //dock->adjustSize();
    //qDebug("%d, %d",dock->widget()->size().height(), instrumentPanel->sizeHint().height());
    //if (dock->height() >= instrumentPanel->sizeHint().height())
        
    //else
    //    this->removeDockWidget(dock);
        
    //if (dock->isAreaAllowed(Qt::RightDockWidgetArea))
    //    this->addDockWidget(Qt::RightDockWidgetArea, dock);
}


