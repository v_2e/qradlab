#include "instrument.h"
#include "unit.h"
#include "connecter.h"

Instrument::Instrument(QWidget *parent): Unit(parent)
{
    panel = new QWidget();
    connecter = new Connecter(Connecter::IN, this);
   // connecter->setGeometry(80,80,15,15);
    connecter->move(13,85);
}

Instrument::~Instrument()
{
 //  qDebug("Instrument destructor \n");
   QWidget *panelParent = (QWidget *)this->panel->parentWidget(); // DAMN WORKS!!!
   if (panelParent) panelParent->close();
   delete this->panel;
   delete connecter;
}

void Instrument::mouseDoubleClickEvent(QMouseEvent *doubleClick)
{
    //this->panelShow();
    if (this->type == Instrument::SPECTROMETER)
        emit dockMe(this->panel,QString("Spectrometer"));
    else if (this->type == Instrument::COUNTER)
        emit dockMe(this->panel,QString("Counter"));
}

void Instrument::setDetector(Detector *detector_)
{
    detector = detector_;
}

void Instrument::panelShow()
{
    this->panel->show();
}
