#include "isotope.h"
#include <QDebug>

Isotope::Isotope()
{

}

Isotope::Isotope(float halflife, float m0, int linesNumber, Isotope::Line *lines)
{
    linesNumber_ = linesNumber;
    lines_ = new Line[linesNumber_];
    for(int i = 0; i < linesNumber_; i++){
        lines_[i] = lines[i];
    }
    halflife_ = halflife;
}

Isotope::~Isotope()
{
    delete []lines_;
}

float Isotope::mass()
{
    return mass_;
}


float Isotope::halflife()
{
    return halflife_;
}

int Isotope::spectralLinesNumber()
{
    return linesNumber_;
}

Isotope::Line Isotope::getSpectralLine(int number)
{
    if(number >=0 && number < linesNumber_){
        return lines_[number];
    }
}
