#ifndef TOOLICONWIDGET_H
#define TOOLICONWIDGET_H
#include <QWidget>

class ToolIconWidget: public QWidget
{
    Q_OBJECT
public:
    explicit ToolIconWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *pe);
    void setIcon(QString path,QSize iconSize);
    void setText(QString text_);

private:
    QImage icon;
    QString text;
};

#endif // TOOLICONWIDGET_H
