#ifndef ISOTOPE_H
#define ISOTOPE_H
#include <QString>

class Isotope
{
public:
    struct Line{
        float energy;
        float quantaDecay;
    };
    enum Notation {Cs137, Na22};

    Isotope();
    Isotope(float halflife, float m0, int linesNumber, Line *lines);
    ~Isotope();

    float mass();
    float halflife();
    int spectralLinesNumber();
    Line getSpectralLine(int number);
    Notation notation;

private:
    int linesNumber_;
    float halflife_;
    Line *lines_;
    float mass_;


};

#endif // ISOTOPE_H
