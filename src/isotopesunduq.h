#ifndef ISOTOPESUNDUQ_H
#define ISOTOPESUNDUQ_H
#include "isotope.h"
#include <QComboBox>

class IsotopeSunduq
{
public:

    IsotopeSunduq();
    ~IsotopeSunduq();


    void getIsotopesList(QComboBox *box);
    Isotope *getIsotope(Isotope::Notation notation);



private:
    int m_iNumber;
    Isotope **list;



};

#endif // ISOTOPESUNDUQ_H
