/*
    Copyright (c) 2014 Vladimir Smolyar <v_2e@ukr.net>

    This file is a part of QRadLab program.

    QRadLab is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>

#include <QWidget>
#include <QLabel>
#include <QPainter>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QMessageBox>

#include "instrument.h"
#include "spectrometer.h"
#include "spectrometer_screen.h"

Spectrometer::Spectrometer(QWidget *parent):
    Instrument(parent),
    numChannels(256),
    mousePressed(false)
{
    this->type = Instrument::SPECTROMETER;

    int channel;
    //Spectrum = (unsigned long int *) malloc( numChannels * sizeof(unsigned long int) ) ;
    Spectrum = (float *) malloc( numChannels * sizeof(float) ) ;
    
    for (channel=0; channel < numChannels; channel++)
        Spectrum[channel] = 0;
    
    Screen = new SpectrometerScreen(panel,this->numChannels);
    
    QVBoxLayout *layout = new QVBoxLayout(panel);
    layout->setSpacing(1);
    layout->addWidget(Screen,Qt::AlignTop);
    
    integralCheck = new QCheckBox(tr("Integral mode"));
    integralCheck->setChecked(false);
    logCheck = new QCheckBox(tr("Log scale"));
    logCheck->setChecked(this->Screen->getLogScale());
    connect(logCheck,SIGNAL(stateChanged(int)),
            this,SLOT(setLogScale(int)));
    
    QHBoxLayout *hlayout = new QHBoxLayout();
    
    // Checkboxes
    QVBoxLayout *smallVlayout = new QVBoxLayout(panel);
    smallVlayout->addWidget(integralCheck);
    smallVlayout->addWidget(logCheck);
    hlayout->addLayout(smallVlayout);
    
    // Upper limit dial
    limitDial = new QDial();
    limitDial->setRange(1,9);
    limitDial->setValue(6);
    limitDial->setSingleStep(1);
    limitDial->setPageStep(2);
    limitDial->setFixedSize(40,40);
    limitDial->setToolTip("Maximum counts in each channel");
    limitDial->setNotchesVisible(true);
    limitDial->setWrapping(false);
    QLabel *label = new QLabel("Lim.");
    smallVlayout = new QVBoxLayout(panel);
    smallVlayout->addWidget(label,Qt::AlignHCenter);
    smallVlayout->addWidget(limitDial,Qt::AlignHCenter);
    hlayout->addLayout(smallVlayout);
        
    // Channels number dial
    channelsDial = new QDial();
    channelsDial->setRange(10,1000);
    channelsDial->setValue(256);
    channelsDial->setSingleStep(10);
    channelsDial->setPageStep(100);
    channelsDial->setToolTip("Number of channels");
    channelsDial->setFixedSize(40,40);
    channelsDial->setWrapping(false);
    channelsDial->setNotchesVisible(true);
    label = new QLabel("Ch.");
    smallVlayout = new QVBoxLayout(panel);
    smallVlayout->addWidget(label,Qt::AlignHCenter);
    smallVlayout->addWidget(channelsDial,Qt::AlignHCenter);
    hlayout->addLayout(smallVlayout);
    
    
    QGroupBox *groupBox = new QGroupBox(tr("Controls"));
    groupBox->setLayout(hlayout);
    groupBox->setMaximumHeight(groupBox->sizeHint().height());
    
    layout->addWidget(groupBox,Qt::AlignBottom);
    
    //layout->setSizeConstraint(QLayout::SetFixedSize);

    connect(this->channelsDial,SIGNAL(sliderPressed()),
            this,SLOT(mousePressedOn()));
    connect(this->channelsDial,SIGNAL(valueChanged(int)),
            this,SLOT(numChannelsChanged(int)));
    connect(this->channelsDial,SIGNAL(sliderReleased()),
            this,SLOT(mousePressedOff()));
    connect(this->limitDial,SIGNAL(valueChanged(int)),
            this,SLOT(setUpperLimit(int)));

     setIcon(QString(":/icons/spectrometer-icon.svg"),QSize(150,120));
}

Spectrometer::~Spectrometer()
{
    //qDebug("Spectrometer destructor");

    delete Screen;
    delete integralCheck;
    delete logCheck;
    //delete clearButton;
    free(Spectrum);
    //qDebug("Spectrometer destructor end");
}

void Spectrometer::refresh()
{
    //qDebug("Spectrometer::refresh");
    this->Screen->DrawSpectrum(this->numChannels, Spectrum);
}

void Spectrometer::addSpectrum(int channels, float* newSpectrum) // is 'channels' needed here?
{
    //qDebug("Spectrometer::addSpectrum");
    // TODO: check if the number of channels is correct
    if (this->integralCheck->isChecked())
        for (int channel=0; channel < this->numChannels; channel++)
        {
            //this->Spectrum[channel] += round(newSpectrum[channel]);
            this->Spectrum[channel] += newSpectrum[channel];
        }
    else
        for (int channel=0; channel < this->numChannels; channel++)
        {
            //this->Spectrum[channel] = round(newSpectrum[channel]);
            this->Spectrum[channel] = newSpectrum[channel];
        }
}

void Spectrometer::numChannelsChanged(int newNumChannels)
{
    //qDebug("Spectrometer::numChannelsChanged: %d",newNumChannels);
    
    // TODO: make sure the user really wants to reset the spectrum
    
    //this->checkNumChannels();
    
    //this->setStatusTip("StatusTipText");
    
    if (mousePressed)
    {
        this->Screen->drawSpectrumEdge(newNumChannels);
        this->Screen->update();
    }
    else
    {
        this->checkNumChannels();
    }
}

void Spectrometer::checkNumChannels()
{
    //qDebug("Spectrometer::checkNumChannels");
    
    int newNumChannels = this->channelsDial->value();
    if (this->numChannels != newNumChannels)
    {
        // Ask if the user want to change the number of channels
        //QMessageBox msgBox;
        //msgBox.setText("Are you sure?"); // TODO: Why this stupid phrase?
        //msgBox.setInformativeText("Changing the number of channels will erase the current values");
        //msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        //msgBox.setDefaultButton(QMessageBox::No);

        //if (msgBox.exec() != QMessageBox::Yes)
        //{
            //this->channelsDial->setValue(this->numChannels);
            //return;
        //}
        
        // Allocate memory for the new spectrum
        //qDebug("Allocate memory for the new spectrum");
        float *newSpectrum = (float *) malloc( newNumChannels * sizeof(float) ) ;
        
        int channel;
        for (channel=0; channel < newNumChannels; channel++)
            newSpectrum[channel] = 0;

        free(this->Spectrum);
        this->Spectrum = newSpectrum;
        this->numChannels = newNumChannels;
        
        this->refresh(); // TODO: stretch or squeeze the existing spectrum -- UPD: impossible?
    }
}

void Spectrometer::mousePressedOn()
{
    //qDebug("Spectrometer::mousePressedOn");
    this->mousePressed = true;
}

void Spectrometer::mousePressedOff()
{
    //qDebug("Spectrometer::mousePressedOff");
    this->mousePressed = false;
    this->checkNumChannels();
}

void Spectrometer::setLogScale(int state)
{
    //qDebug("Spectrometer::setLogScale");
    if (state == Qt::Unchecked)
        this->Screen->setLogScale(false);
    else
        this->Screen->setLogScale(true);
    
    this->refresh();
}

void Spectrometer::setUpperLimit(int limit)
{
    //qDebug("Spectrometer::setUpperLimit");
    this->Screen->setUpperLimit(limit);
    this->refresh();
}

void Spectrometer::showProperties()
{
    properties->show();
}

void Spectrometer::closeProperties()
{
}
