#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QMenu>
#include <QAction>
#include <qmath.h>
#include <QList>
#include <QMessageBox>

#include "workspace.h"
#include "backgroundsettings.h"
#include "unit.h"
#include "detector.h"
#include "instrument.h"
#include "source.h"
#include "absorber.h"
#include <QPen>
#include "isotopesunduq.h"

#include <QDebug>

Workspace::Workspace(QWidget *parent):
    QWidget(parent),
    scale(0.05)
{
    bgsettings = new BackgroundSettings(this);
    bgsettings->setModal(true);
    connect(bgsettings,SIGNAL(applied()),this,SLOT(applySettings()));
    unitsCounter = 0;
    cursorHighlighting = true;
    background = new QImage(size(),QImage::Format_RGB32);

    //scale =50;


    contextMenu = new QMenu(this);
    // Next commands adds different actions to the context menu;
      actionAllLock = new QAction(tr("Lock all"),this);
        contextMenu->addAction(actionAllLock);
        connect(actionAllLock,SIGNAL(triggered()),this,SLOT(allLock()));

      actionAllUnlock = new QAction(tr("Unlock all"),this);
        contextMenu->addAction(actionAllUnlock);
        connect(actionAllUnlock,SIGNAL(triggered()),this,SLOT(allUnlock()));

     actionDeleteAll = new QAction(tr("Delete all"),this);
        contextMenu->addAction(actionDeleteAll);
        connect(actionDeleteAll,SIGNAL(triggered()),this,SLOT(deleteAllRequest()));

        actionCursorHighlight = new QAction(tr("Cursor highlight 'on/off'"),this);
        contextMenu->addAction(actionCursorHighlight);
        connect(actionCursorHighlight,SIGNAL(triggered()),this,SLOT(setCursorHighlighing()));


    units.detectors.clear();
    units.absorbers.clear();
    units.instruments.clear();
    units.sources.clear();
    
    WorkingTimer = new QTimer();
    WorkingTimer->setInterval(1000);
    connect(WorkingTimer, SIGNAL(timeout()), this, SLOT(Simulation()));  

    board = new WiringBoard(this);
    board->move(0,0);
    board->resize(this->width(),this->height());
    board->show();
    board->setForkIcon(QString(":/icons/wire_end-icon.svg"),QSize(13,13));

    sunduq = new IsotopeSunduq();

}

Workspace::~Workspace() {
  //  qDebug("Workspace destructor \n");
    delete board;
    deleteAll();
}

void Workspace::paintEvent(QPaintEvent *paint)
{
    QPainter p(this);
    p.drawImage(0,0,*background);
    if(cursorHighlighting){
        cursorHighlight(cursor_x,cursor_y);
    }
}

void Workspace::resizeEvent(QResizeEvent *resize)
{
    delete background;
    background = new QImage(resize->size(),QImage::Format_RGB32);
    //*background = background->scaled(resize->size(),Qt::IgnoreAspectRatio,Qt::FastTransformation);

    backgroundFill();

    board->resize(width(),height());
    emit resized();
}

void Workspace::mouseReleaseEvent(QMouseEvent *mouse)
{
    if(mouse->button() == Qt::RightButton){
      contextMenu->exec(mouse->globalPos());
    }
}

void Workspace::mouseMoveEvent(QMouseEvent *moved)
{
    if(cursorHighlighting){
    cursor_x = moved->x();
    cursor_y = moved->y();
    update();
    }
    emit cursorX(moved->x());
    emit cursorY(moved->y());
}

void Workspace::mousePressEvent(QMouseEvent *pressed)
{
    this->setFocus();
}

void Workspace::cursorHighlight(int x, int y)
{
    QPainter p(this);
    QPen pen(Qt::darkRed);
    pen.setStyle(Qt::DashLine);
    pen.setWidth(2);
    p.setPen(pen);
    p.drawLine(0,y,this->width(),y);
    p.drawLine(x,0,x,this->height());
}

void Workspace::backgroundFill()
{
    gridStep = bgsettings->finalSettings.gridStep;
    if(!background->isNull()){
    QPainter p;
    p.begin(background);
    p.setRenderHint(QPainter::Antialiasing,false);
    QBrush brush(bgsettings->finalSettings.backgroundColor);
    QPen pen;
    pen.setColor(QColor(bgsettings->finalSettings.backgroundColor));
    p.setBrush(brush);
    p.setPen(pen);
    p.drawRect(0,0,this->width(),this->height());

    if(bgsettings->finalSettings.grid){

        brush.setColor(bgsettings->finalSettings.gridColor);
        pen.setColor(QColor(bgsettings->finalSettings.gridColor));

        if(this->bgsettings->finalSettings.gridWidth){
            int x,y,gridStep;
            pen.setWidth(this->bgsettings->finalSettings.gridWidth);
            p.setBrush(brush);
            p.setPen(pen);

            gridStep = bgsettings->finalSettings.gridStep;
            x = gridStep;
            y = gridStep;

            // draw vertical grid lines
            while(x < this->width()){
                p.drawLine(x,0,x,this->height());
                x=x+gridStep;
            }

            // draw horizontal grid lines
            while(y < this->height()){
                p.drawLine(0,y,this->width(),y);
                y=y+gridStep;
            }
        }

    }
    else if(bgsettings->finalSettings.dots){

        brush.setColor(bgsettings->finalSettings.gridColor);
        pen.setColor(QColor(bgsettings->finalSettings.gridColor));
        if(this->bgsettings->finalSettings.gridWidth){
            int x,y,gridStep;
            pen.setWidth(this->bgsettings->finalSettings.gridWidth);
            p.setBrush(brush);
            p.setPen(pen);

            gridStep = bgsettings->finalSettings.gridStep;
            x = gridStep;
            y = gridStep;

            // draw vertical grid lines
            while(x < this->width()){
                y = 0;
                while(y < this->height()){
                    //   p.drawLine(0,y,this->width(),y);
                    p.drawEllipse(x,y,this->bgsettings->finalSettings.gridWidth,this->bgsettings->finalSettings.gridWidth);
                    y=y+gridStep;
                }
                x=x+gridStep;
            }
        }

    }

   // qDebug() << background->width() << background->height();
    p.end();
   }
}

void Workspace::refresh()
{
    this->update();
}

void Workspace::applySettings()
{
    for(int i=0;i<units.detectors.size();i++){
       units.detectors.at(i)->stickingStep = bgsettings->finalSettings.gridStep;
    }
    for(int i=0;i<units.sources.size();i++){
       units.sources.at(i)->stickingStep = bgsettings->finalSettings.gridStep;
    }
    for(int i=0;i<units.instruments.size();i++){
       units.instruments.at(i)->stickingStep = bgsettings->finalSettings.gridStep;
    }
    for(int i=0;i<units.absorbers.size();i++){
       units.absorbers.at(i)->stickingStep = bgsettings->finalSettings.gridStep;
    }

    backgroundFill();
}

void Workspace::allLock()
{
    for(int i=0;i<units.detectors.size();i++){
        units.detectors.at(i)->locked = true;
    }

    for(int i=0;i<units.sources.size();i++){
        units.sources.at(i)->locked = true;
    }

    for(int i=0;i<units.instruments.size();i++){
        units.instruments.at(i)->locked = true;
    }

    for(int i=0;i<units.absorbers.size();i++){
        units.absorbers.at(i)->locked = true;
    }
}

void Workspace::allUnlock()
{
    for(int i=0;i<units.detectors.size();i++){
        units.detectors.at(i)->locked = false;
    }

    for(int i=0;i<units.sources.size();i++){
        units.sources.at(i)->locked = false;
    }

    for(int i=0;i<units.instruments.size();i++){
        units.instruments.at(i)->locked = false;
    }

    for(int i=0;i<units.absorbers.size();i++){
        units.absorbers.at(i)->locked = false;
    }
}

void Workspace::deleteAllRequest()
{
    QMessageBox msgBox;
    msgBox.setText("Are you sure?");
    msgBox.setInformativeText("Do you really want to delete ALL the units?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes)
        this->deleteAll();
}

void Workspace::setCursorHighlighing()
{
    cursorHighlighting = !cursorHighlighting;
    update();
}

void Workspace::onUnitCursorMoved()
{
    Unit *u = qobject_cast<Unit*>(Workspace::sender());
    int x = u->x() + u->width()/2;
    int y = u->y()+u->height()/2;
      cursor_x = x;
      cursor_y = y;
    emit cursorX(x);
    emit cursorY(y);
    update();




//    qDebug(u->metaObject()->className());
//    if(u->metaObject()->className() == "QObject"){
//        qDebug("Uuuu");
//    }


}


void Workspace::moveUnit(QPoint toPoint)
{
   Unit *unit =  qobject_cast<Unit*>(Workspace::sender());
    int new_x = toPoint.x();
    int new_y = toPoint.y();
//    int unitId = unit->id;

//    bool canBeReplaced = true;
//    int nAbrorbers = units.absorbers.size();
//    int nDetectors = units.detectors.size();
//    int nSources = units.sources.size();
//    int nInstruments = units.instruments.size();


//    int left_x = unit->x();
//    int left_y = unit->y();
//    int right_x = unit->x() + unit->width();
//    int right_y = unit->y() + unit->height();
//    static int counter1 = 0;
//    counter1++;

//    // ABSORBERS
//    for(int i = 0; i < nAbrorbers; i++ ){
//        if(units.absorbers.at(i)->id != unitId){
//            int left2_x = units.absorbers.at(i)->x();
//            int left2_y = units.absorbers.at(i)->y();
//            int right2_x = units.absorbers.at(i)->x() + units.absorbers.at(i)->width();
//            int right2_y = units.absorbers.at(i)->y() + units.absorbers.at(i)->height();

//            int cover_x = left_x <= left2_x ? right_x - left2_x : right2_x - left_x;
//            int cover_y = left_y <= left2_y ? right_y - left2_y : right2_y - left_y;


//            // FROM LEFT
//            if((right_x > left2_x && right_x < right2_x) && (right_y > left2_y && left_y < right2_y)){
//                canBeReplaced = false;
//                if(cover_y > cover_x){
//                    unit->move(left2_x - unit->width(), left_y);
//                }
//            }

//            // FROM RIGHT
//            if((left_x < right2_x && left_x > left2_x) && (right_y > left2_y && left_y < right2_y)){
//                canBeReplaced = false;
//                if(cover_y > cover_x){
//                    unit->move(right2_x, left_y);
//                }
//            }

//            // FROM TOP
//            if((right_y > left2_y && right_y < right2_y) && (right_x > left2_x && left_x < right2_x)){
//                canBeReplaced = false;
//                if(cover_y <= cover_x){
//                    unit->move(left_x, left2_y - unit->height());
//                }
//            }



//            // FROM BOTTOM
//            if((left_y < right2_y && left_y > left2_y) && (right_x > left2_x && left_x < right2_x)){
//                canBeReplaced = false;
//                if(cover_y <= cover_x){
//                    unit->move(left_x, right2_y);
//                }
//            }


//        }
//    }

//    //DETECTORS

//    for(int i = 0; i < nDetectors; i++ ){
//        if(units.detectors.at(i)->id != unitId){
//            int left2_x = units.detectors.at(i)->x();
//            int left2_y = units.detectors.at(i)->y();
//            int right2_x = units.detectors.at(i)->x() + units.detectors.at(i)->width();
//            int right2_y = units.detectors.at(i)->y() + units.detectors.at(i)->height();

//            int cover_x = left_x <= left2_x ? right_x - left2_x : right2_x - left_x;
//            int cover_y = left_y <= left2_y ? right_y - left2_y : right2_y - left_y;


//            // FROM LEFT
//            if((right_x > left2_x && right_x < right2_x) && (right_y > left2_y && left_y < right2_y)){
//                canBeReplaced = false;
//                if(cover_y > cover_x){
//                    unit->move(left2_x - unit->width(), left_y);
//                }
//            }

//            // FROM RIGHT
//            if((left_x < right2_x && left_x > left2_x) && (right_y > left2_y && left_y < right2_y)){
//                canBeReplaced = false;
//                if(cover_y > cover_x){
//                    unit->move(right2_x, left_y);
//                }
//            }

//            // FROM TOP
//            if((right_y > left2_y && right_y < right2_y) && (right_x > left2_x && left_x < right2_x)){
//                canBeReplaced = false;
//                if(cover_y <= cover_x){
//                    unit->move(left_x, left2_y - unit->height());
//                }
//            }



//            // FROM BOTTOM
//            if((left_y < right2_y && left_y > left2_y) && (right_x > left2_x && left_x < right2_x)){
//                canBeReplaced = false;
//                if(cover_y <= cover_x){
//                    unit->move(left_x, right2_y);
//                }
//            }
//        }
//    }

//    //SOURCES
//    for(int i = 0; i < nSources; i++ ){
//        if(units.sources.at(i)->id != unitId){

//        }
//    }

//    //INSTRUMENTS
//    for(int i = 0; i < nInstruments; i++ ){
//        if(units.instruments.at(i)->id != unitId){

//        }
//    }

  // if(canBeReplaced)
       unit->move(toPoint);


}

void Workspace::deleteAll()
{
    while(units.detectors.size()){
    delete units.detectors.at(0);
            units.detectors.removeAt(0);
    }

    while(units.sources.size()){
            delete units.sources.at(0);
            units.sources.removeAt(0);
    }

    while(units.instruments.size()){
            delete units.instruments.at(0);
            units.instruments.removeAt(0);
    }

    while(units.absorbers.size()){
            delete units.absorbers.at(0);
            units.absorbers.removeAt(0);
    }
}

void Workspace::createSource()
{
    Source* source = new Source(this,sunduq);
    unitsCounter++;
    source->id = unitsCounter;
    source->stickingStep = bgsettings->finalSettings.gridStep;
    source->color=Qt::yellow;
    source->setFocus();
    source->setGeometry(qrand()%(this->width()-110), qrand()%(this->height()-110),100,100);
    source->show();
    units.sources.append(source);
    source->relative_x = double(source->x()+source->width())/this->width();
    source->relative_y = double(source->y()+source->height())/this->height();
    connect(this,SIGNAL(resized()),source,SLOT(keepPosInWorkspace()));
    connect(source,SIGNAL(removeSignal(int)),this,SLOT(deleteUnit(int)));
    connect(source,SIGNAL(moved(int)),this,SLOT(onUnitCursorMoved()));
    connect(source,SIGNAL(tryMove(QPoint)),this,SLOT(moveUnit(QPoint)));
}

void Workspace::createAbsorber()
{
    Absorber *absorber = new Absorber(this);
    unitsCounter++;
    absorber->id = unitsCounter;
    absorber->stickingStep = bgsettings->finalSettings.gridStep;
    absorber->color=Qt::gray;
    absorber->setFocus();
    absorber->setGeometry(qrand()%(this->width()-110), qrand()%(this->height()-110),150,150);
    absorber->show();
    units.absorbers.append(absorber);
    absorber->relative_x = double(absorber->x()+absorber->width())/this->width();
    absorber->relative_y = double(absorber->y()+absorber->height())/this->height();
    connect(this,SIGNAL(resized()),absorber,SLOT(keepPosInWorkspace()));
    connect(absorber,SIGNAL(removeSignal(int)),this,SLOT(deleteUnit(int)));
    connect(absorber,SIGNAL(moved(int)),this,SLOT(onUnitCursorMoved()));
    connect(absorber,SIGNAL(tryMove(QPoint)),this,SLOT(moveUnit(QPoint)));
}

void Workspace::createDetector()
{
    Detector* detector = new Detector(this);
    unitsCounter++;
    detector->id = unitsCounter;
    detector->stickingStep = bgsettings->finalSettings.gridStep;
    detector->color=Qt::green;
    detector->setFocus();
    detector->setGeometry(qrand()%(this->width()-110), qrand()%(this->height()-110),160,120);
    detector->size = 5; //TODO  real detector size
    detector->show();
    units.detectors.append(detector);
    detector->relative_x = double(detector->x()+detector->width())/this->width();
    detector->relative_y = double(detector->y()+detector->height())/this->height();
    connect(this,SIGNAL(resized()),detector,SLOT(keepPosInWorkspace()));
    connect(detector,SIGNAL(removeSignal(int)),this,SLOT(deleteUnit(int)));
    connect(detector,SIGNAL(moved(int)),this,SLOT(onUnitCursorMoved()));
      // Wire-building connection:
    connect(detector->connecter,SIGNAL(activated()),board,SLOT(connecterActivated()));
    connect(detector->connecter,SIGNAL(mouseMoved(QPoint)),this,SLOT(lookingForConnecter(QPoint)));
    connect(detector->connecter,SIGNAL(stopConnection()),board,SLOT(connectionCut()));
    connect(detector->connecter,SIGNAL(released()),this,SLOT(tryConnect()));
    connect(detector,SIGNAL(tryMove(QPoint)),this,SLOT(moveUnit(QPoint)));

}

Instrument *Workspace::createCounter()
{
    Counter* instrument = new Counter(this);
    unitsCounter++;
    instrument->id = unitsCounter;
    instrument->stickingStep = bgsettings->finalSettings.gridStep;
    instrument->color=Qt::white;
    instrument->setFocus();
    instrument->setGeometry(qrand()%(this->width()-110), qrand()%(this->height()-110),150,120);
    instrument->show();
    units.instruments.append(instrument);
    instrument->relative_x = double(instrument->x()+instrument->width())/this->width();
    instrument->relative_y = double(instrument->y()+instrument->height())/this->height();

    connect(this,SIGNAL(resized()),instrument,SLOT(keepPosInWorkspace()));
    connect(instrument,SIGNAL(removeSignal(int)),this,SLOT(deleteUnit(int)));
    connect(instrument,SIGNAL(moved(int)),this,SLOT(onUnitCursorMoved()));


    // Wire-building connection:
    connect(instrument->connecter,SIGNAL(activated()),board,SLOT(connecterActivated()));
    connect(instrument->connecter,SIGNAL(mouseMoved(QPoint)),this,SLOT(lookingForConnecter(QPoint)));
    connect(instrument->connecter,SIGNAL(stopConnection()),board,SLOT(connectionCut()));
    connect(instrument->connecter,SIGNAL(released()),this,SLOT(tryConnect()));
    connect(instrument,SIGNAL(tryMove(QPoint)),this,SLOT(moveUnit(QPoint)));




    return instrument;
}

Instrument *Workspace::createSpectrometer()
{
    Spectrometer* instrument = new Spectrometer(this);
    unitsCounter++;
    instrument->id = unitsCounter;
    instrument->stickingStep = bgsettings->finalSettings.gridStep;
    instrument->color=Qt::darkCyan;
    instrument->setFocus();
    instrument->setGeometry(qrand()%(this->width()-110), qrand()%(this->height()-110),150,120);
    instrument->show();
    units.instruments.append(instrument);
    instrument->relative_x = double(instrument->x()+instrument->width())/this->width();
    instrument->relative_y = double(instrument->y()+instrument->height())/this->height();
    connect(this,SIGNAL(resized()),instrument,SLOT(keepPosInWorkspace()));
    connect(instrument,SIGNAL(removeSignal(int)),this,SLOT(deleteUnit(int)));
    connect(instrument,SIGNAL(moved(int)),this,SLOT(onUnitCursorMoved()));
    // Wire-building connection:
    connect(instrument->connecter,SIGNAL(activated()),board,SLOT(connecterActivated()));
    connect(instrument->connecter,SIGNAL(mouseMoved(QPoint)),this,SLOT(lookingForConnecter(QPoint)));
    connect(instrument->connecter,SIGNAL(stopConnection()),board,SLOT(connectionCut()));
    connect(instrument->connecter,SIGNAL(released()),this,SLOT(tryConnect()));
    connect(instrument,SIGNAL(tryMove(QPoint)),this,SLOT(moveUnit(QPoint)));

    return instrument;
}

void Workspace::selectUnit(Unit *unit)
{

}

Unit *Workspace::selectedUnit()
{

}

void Workspace::deleteUnit(int ID)
{
    for(int i=0;i<units.detectors.size();i++){
        if(units.detectors.at(i)->id == ID){
            delete units.detectors.at(i);
            units.detectors.removeAt(i);
            return;
        }
    }

    for(int i=0;i<units.sources.size();i++){
        if(units.sources.at(i)->id == ID){
            delete units.sources.at(i);
            units.sources.removeAt(i);
            return;
        }
    }

    for(int i=0;i<units.instruments.size();i++){
        if(units.instruments.at(i)->id == ID){
            delete units.instruments.at(i);
            units.instruments.removeAt(i);
            return;
        }
    }

    for(int i=0;i<units.absorbers.size();i++){
        if(units.absorbers.at(i)->id == ID){
            delete units.absorbers.at(i);
            units.absorbers.removeAt(i);
            return;
        }
    }

    this->setFocus();
}

void Workspace::calculateTransmissionMatrix(int numSources, int numDetectors, float **TransmissionMatrix)
{
    for(int i=0;i<numSources;i++)
        for(int j=0;j<numDetectors;j++)
        {
            float xd,xs,yd,ys,r;
            xd=this->units.detectors.at(j)->x_cm(scale);
            yd=this->units.detectors.at(j)->y_cm(scale);
            xs=this->units.sources.at(i)->x_cm(scale);
            ys=this->units.sources.at(i)->y_cm(scale);
            r=sqrt(pow((xd-xs),2)+pow((yd-ys),2));
            TransmissionMatrix[i][j]=pow(this->units.detectors.at(j)->size,2)/(16*(pow(r,2)));
        }
    
    if (this->units.absorbers.size() > 0)
    for(int i=0;i<numSources;i++)
        for(int j=0;j<numDetectors;j++)
        {
            float xd,xs,yd,ys,k,b,a,g,c,xa,ya;
            xd=this->units.detectors.at(j)->x_cm(scale);
            yd=this->units.detectors.at(j)->y_cm(scale);
            xs=this->units.sources.at(i)->x_cm(scale);
            ys=this->units.sources.at(i)->y_cm(scale);
            k = (yd-ys)/(xd-xs);
            b = ys-k*xs;


            for(int l=0;l<this->units.absorbers.size();l++)
            {

                xa = this->units.absorbers.at(l)->x_cm(scale);
                ya = this->units.absorbers.at(l)->y_cm(scale);
                float aRad = this->units.absorbers.at(l)->size/2;
                if ((xa > fmax(xs+aRad,xd+aRad)) || 
                    (xa < fmin(xs-aRad,xd-aRad)) ||
                    (ya > fmax(ys+aRad,yd+aRad)) || 
                    (ya < fmin(ys-aRad,yd-aRad)))
                    continue;
                
                if (xs == xd) // Source and detector are along a vertical line
                {
                    a = xa-xd;
                    //qDebug("xs = xd");
                    //printf("xs = xd; a = %f\n",a);
                }
                else
                {
                    //qDebug("xs != xd");
                    g = M_PI_2 - atan(k);
                    c = (k*xa+b-ya)*sin(g);
                    a = c*sin(g);
                }

                if (abs(a) < this->units.absorbers.at(l)->size/2)
                {
                   TransmissionMatrix[i][j] *= 0.1;
                }
            }
         }
    //else qDebug("No absorbers.");
}

void Workspace::startSimulation()
{
   WorkingTimer->start();    
}

void Workspace::stopSimulation()
{
    WorkingTimer->stop();
}

void Workspace::lookingForConnecter(QPoint point)
{
    Connecter *client = qobject_cast<Connecter*>(Workspace::sender());
    QWidget *prnt = qobject_cast<QWidget*>(client->parentWidget());
    QPoint real(point.x() + prnt->x() + client->x(),point.y() + prnt->y() + client->y());

    cursor_x = real.x();
    cursor_y = real.y();
    update();

    board->setCursorPos(real);
}

void Workspace::tryConnect()
{
    QPoint point = board->getCursorPos();
   // emit cursorX(point.x());
  //  emit cursorY(point.y());


    for(int i=0;i<units.instruments.size();i++){
        Connecter *current = units.instruments.at(i)->connecter;
        QWidget *instrument = current->parentWidget();
        if(instrument->x() + current->x() < point.x() && instrument->x() + current->x() + current->width() > point.x()){
         if(instrument->y() + current->y() < point.y() && instrument->y() + current->y() + current->height() > point.y()){
             qDebug("+");
             current->activate();
             return;
         }
        }
    }



    for(int i=0;i<units.detectors.size();i++){
        Connecter *current = units.detectors.at(i)->connecter;
        QWidget *instrument = current->parentWidget();
        if(instrument->x() + current->x() < point.x() && instrument->x() + current->x() + current->width() > point.x()){
         if(instrument->y() + current->y() < point.y() && instrument->y() + current->y() + current->height() > point.y()){
             qDebug("+");
             current->activate();
             return;
         }
        }
    }
    board->resume();
    board->update();

    return;
}

float gauss2(float x, float mu, float sigma)
{
    return (1/(sigma*sqrt(2*M_PI))) * exp(-powf((x-mu),2)/(2*sigma*sigma));
}

void Workspace::Simulation()
{
    // TODO
    // Check
    int numSources = this->units.sources.size();
    int numDetectors = this->units.detectors.size();
    
    //if ((numDetectors == 0) || (numSources == 0)) return; // Seems to work correctly even without this line
    
    // Create transmission matrix
    float **TransmissionMatrix;
    TransmissionMatrix = (float**)malloc( numSources * sizeof(float*) ) ;
    // Initialize the transmission matrix
    for (int s=0; s < numSources; s++)
    {
        TransmissionMatrix[s] = (float*)malloc( numDetectors * sizeof(float) );
        for(int d=0; d < numDetectors; d++) TransmissionMatrix[s][d] = 0.0;
    }

    this->calculateTransmissionMatrix(numSources, numDetectors, TransmissionMatrix);


    // Process    
    if (this->units.instruments.size() > 0)
    {
        Instrument *selectedInstrument;
        
        int d; // Detector number within the array of detectors
        
        for (int i=0; i < this->units.instruments.size(); i++)
        {
            selectedInstrument = NULL;
            
            d = -1; // No detector connected
            
            // Check if some detector is connected to the instrument.
            // Switch to the next one otherwise.
            for(int j=0;j<numDetectors;j++)
            {
                if (this->units.instruments.at(i)->getDetector() == this->units.detectors.at(j)) 
                {
                    //qDebug("Detector pointer found. Detector #%d\n",j);
                    d = j; // Detector number is found to be j
                    break;
                }
                //else qDebug("Detector %d is not the right one.\n",j);
            }
            
            //if (d < 0) continue; // Jump to next instrument if no detectors connected
            
            if (this->units.instruments.at(i)->type == Instrument::COUNTER)
            {
                // Selected instrument is a COUNTER
                Counter* selectedCounter = (Counter*) this->units.instruments.at(i);
                
                unsigned long int counts = 0;
                if (d >= 0) // there is a connected detector
                {
                    for(int s=0; s < numSources; s++)
                    {
                        counts += this->units.sources.at(s)->activity*TransmissionMatrix[s][d];
                    }
                 }
                selectedCounter->addParticles(counts);
                selectedInstrument = selectedCounter;
            }
            else if (this->units.instruments.at(i)->type == Instrument::SPECTROMETER)
            {
                // Selected instrument is a SPECTROMETER
            
                Spectrometer* selectedSpectrometer = (Spectrometer*) this->units.instruments.at(i);
                                            
                float mu, sigma;
                
                // Create a temporary spectrum with FLOAT numbers
                // makes the spectrum growth smooth
                float *spectrum = (float *) malloc( selectedSpectrometer->numChannels * sizeof(float) ) ;
                for (int channel=0; channel < selectedSpectrometer->numChannels; channel++) 
                    spectrum[channel] = 0;
                
                if (d >= 0) // there is a connected detector        
                for(int s=0; s < numSources; s++)
                {
                    // TODO: max energy should be set in the Spectrometer settings
                    mu = selectedSpectrometer->numChannels * this->units.sources.at(s)->energy / 10; // 10 MeV - max energy
                    
                    // TODO: read energy resolution from detector type
                    sigma = mu*0.05; // Should be detector-dependent
                    
                    //qDebug("Sigma = %f, mu = %f\n",sigma,mu);
                    for (int channel=0; channel < selectedSpectrometer->numChannels; channel++)
                    {
                        //selectedInstrument->Spectrum[channel] += 1000* (1/(sigma*sqrt(2*M_PI))) * exp(-pow((channel-mu),2)/(2*sigma*sigma));
                        //selectedInstrument->Spectrum[channel] += workspace->units.sources.at(i)->activity
                        spectrum[channel] += this->units.sources.at(s)->activity
                                                                   * TransmissionMatrix[s][d]
                                                                   * gauss2((float) channel, mu, sigma);
                    }            
                }
                
                // Copy from temporary FLOAT spectrum to instrument's INT spectrum
                selectedSpectrometer->addSpectrum(selectedSpectrometer->numChannels, spectrum);
                free(spectrum);
                selectedInstrument = selectedSpectrometer;
            }
            
            // Refresh the instrument's screen
            if (selectedInstrument) selectedInstrument->refresh();    
        }
    }    
}
