/*
    Copyright (c) 2014 Vladimir Smolyar <v_2e@ukr.net>

    This file is a part of QRadLab program.

    QRadLab is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QSize>
#include <QImage>
#include <QPainter>
#include <math.h>
#include "spectrometer_screen.h"

//class QResizeEvent;

SpectrometerScreen::SpectrometerScreen(QWidget *parent, int channels):
    QFrame(parent),
    numChannels(channels),
    logScale(true),
    upperLimit(6)
{
    this->setMinimumSize(304, 134);
    this->setSizeIncrement(1,30); // BUG: Does nothing so far
    
    this->setLineWidth(2);
    this->setFrameStyle(QFrame::Sunken | QFrame::Panel);
    this->Image = 0;
    this->Painter = 0;
    if (this->Image == 0) this->Image = new QImage(QSize(300, 220),QImage::Format_RGB32);
    if (this->Painter == 0) this->Painter = new QPainter(this->Image);
    this->Painter->eraseRect(0,0,this->Image->width(),this->Image->height());
    
    //this->DrawGrid();    
    this->Clear();
    
    //delete this->Painter;
}

void SpectrometerScreen::DrawGrid()
{
    this->Painter->setPen(QPen(Qt::yellow, 1, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin));
    //this->Painter->pen()->setColor("#ffff00");
    int width = this->Image->width();
    int height = this->Image->height();
    //qDebug("DrawGrid: %d x %d",width,height);
    this->Painter->drawRect(29,10,width-42,height-39);
    
    // X-axis ticks
    this->Painter->setFont(QFont("Monospace", 10));
    for (int i=0; i < width-29-13; i+=50)
    {
      this->Painter->drawLine(30+i, height-30, 30+i, height-30+7);
      this->Painter->drawText(30+i-10, height-5, QString::number(i));
    }
    
    // Y-axis ticks
    if (this->logScale)
    {
        int yStep = (height-40) / this->upperLimit;
        for (int i=1; i <= this->upperLimit; i++)
        {
          this->Painter->drawLine(23, height-30-i*yStep, 30, height-30-i*yStep);
          this->Painter->setFont(QFont("Monospace", 9));
          this->Painter->drawText(0, height-30-i*yStep+5, "10");
          this->Painter->setFont(QFont("Monospace", 6));
          this->Painter->drawText(15, height-30-i*yStep-3, QString::number(i));
        }
    }
    else
    {
        int yStep = (height-41)/10;
        
        if (this->upperLimit > 2)
        {
            this->Painter->setFont(QFont("Monospace", 9));
            this->Painter->drawText(0, height-30-0*yStep+5, "x10");
            this->Painter->setFont(QFont("Monospace", 6));
            this->Painter->drawText(22, height-30-0*yStep-3, QString::number(this->upperLimit-1));
        }
        else if (this->upperLimit > 1)
        {
            this->Painter->setFont(QFont("Monospace", 9));
            this->Painter->drawText(0, height-30-0*yStep+5, "x10");
        }

        this->Painter->setFont(QFont("Monospace", 9));
        for (int i=1; i<11; i++)
        {
          this->Painter->drawLine(23, height-30-i*yStep, 30, height-30-i*yStep);
          this->Painter->drawText(0, height-30-i*yStep+5, QString::number(i));
        }
    }
    
    // Grid lines
    this->Painter->setPen(QPen(Qt::yellow, 1, Qt::DotLine, Qt::FlatCap, Qt::MiterJoin));
        
    // Horizontal grid lines
    if (this->logScale)
    {
        int yStep = (height-40) / this->upperLimit;
        for (int i=1; i <= this->upperLimit; i++)
        {
            this->Painter->drawLine(30, height-30-i*yStep, width-13, height-30-i*yStep);
        }
    }
    else
    {
        int yStep = (height-41)/10;
        for (int i=1; i<11; i++)
        {
            this->Painter->drawLine(30, height-30-i*yStep, width-13, height-30-i*yStep);
        }
    }
    
    // Vertical grid lines
    for (int i=0; i < width-29-13; i+=50)
    
    {
      this->Painter->drawLine(30+i, 10, 30+i, height-30);
    }
    
    //this->Painter->setPen(Qt::SolidLine);
    this->Painter->setPen("#00ff00");
}


void SpectrometerScreen::Clear()
{
    //qDebug("Clear signal.");
    
    this->Painter->fillRect (0,0,this->Image->width(),this->Image->height(),QBrush("#000000",Qt::SolidPattern));
    
    this->DrawGrid();
    
    this->update();
}

SpectrometerScreen::~SpectrometerScreen()
{
}

void SpectrometerScreen::paintEvent(QPaintEvent *event)
{
	//qDebug("paintEvent");
    QFrame::paintEvent(event);
	QPainter MyWidgetPainter(this); // A painter on the widget itself
    MyWidgetPainter.setRenderHint(QPainter::Antialiasing);
	MyWidgetPainter.drawImage(2,2,*this->Image);
    
    int width = this->Image->width();
    if (this->numChannels < width-29-13)
    {
        // Draw the allocated channels edge
        int height = this->Image->height();
        MyWidgetPainter.setPen(QPen(QColor("#00ffff"), 2, Qt::DotLine, Qt::FlatCap, Qt::MiterJoin));
        MyWidgetPainter.drawLine(30+numChannels, 11+2, 30+numChannels, height-29+2);
    }
    else
    {
        // Draw small arrows to the right
        int height = this->Image->height();
        MyWidgetPainter.setPen(QPen(QColor("#00ffff"), 2, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin));
        MyWidgetPainter.drawLine(width+2-13-10, height/3, width+2-13, height/3);
        MyWidgetPainter.drawLine(width+2-13-6, height/3-3, width+2-13, height/3);
        MyWidgetPainter.drawLine(width+2-13-6, height/3+3, width+2-13, height/3);

        MyWidgetPainter.drawLine(width+2-13-10, 2*height/3, width+2-13, 2*height/3);
        MyWidgetPainter.drawLine(width+2-13-6, 2*height/3-3, width+2-13, 2*height/3);
        MyWidgetPainter.drawLine(width+2-13-6, 2*height/3+3, width+2-13, 2*height/3);
    }
}

void SpectrometerScreen::DrawSpectrum(int channels, float *spectrum)
{
    //qDebug("DrawSpectrum signal.");
    
    this->numChannels = channels;
    this->Clear();
    int width = this->Image->width();
    int height = this->Image->height();
    
    if (this->logScale)
    {    
        float yScale = (height-41) / this->upperLimit;
        
        for (int i=0; i < qMin(numChannels,width-29-13); i++)
        {      
          int amplitude = floor(yScale*log10(spectrum[i]));
          if (amplitude > height-41) amplitude = height-41;
          if (amplitude < 0) amplitude = 0;
          this->Painter->drawLine(30+i, height-30, 30+i, height-30-amplitude);
        }
    }
    else
    {
        float yScale = (height-41)/pow(10,this->upperLimit);
        for (int i=0; i < qMin(numChannels,width-29-13); i++)
        {      
          int amplitude = floor(yScale*spectrum[i]);
          if (amplitude > height-41) amplitude = height-41;
          if (amplitude < 0) amplitude = 0;
          this->Painter->drawLine(30+i, height-30, 30+i, height-30-amplitude);
        }
    }
    
    this->update();
}

void SpectrometerScreen::resizeEvent(QResizeEvent * event)
{
    //qDebug("resizeEvent: %d x %d",event->size().width(),event->size().height());
    QWidget::resizeEvent(event); // What is this for?
    
    if (this->Painter != NULL) delete this->Painter;
    if (this->Image != NULL) delete this->Image;
    this->Image = 
        new QImage(QSize(this->width()-4, this->height()-4), QImage::Format_RGB32);
    this->Painter = new QPainter(this->Image);
    this->Clear();
}

void SpectrometerScreen::drawSpectrumEdge(int maxChannel)
{
    this->numChannels = maxChannel;
    this->update();
}
