#include "isotope.h"
#include "isotopesunduq.h"
#include <QDebug>
#include <QComboBox>

IsotopeSunduq::IsotopeSunduq():
    m_iNumber(0),
    list(0)
{
  //  list = new Isotope*[25];
}

IsotopeSunduq::~IsotopeSunduq()
{
    for(int i = 0; i < m_iNumber; i++){
        delete list[i];
    }
    delete [] list;
}



void IsotopeSunduq::getIsotopesList(QComboBox *box)
{
      box->addItem("Cs137",Isotope::Cs137);
      box->addItem("Na22",Isotope::Na22);

    //   box->addItem("U238",Isotope::U238);
}


Isotope *IsotopeSunduq::getIsotope(Isotope::Notation notation)
{
    Isotope **temp;
    if(m_iNumber != 0){
        // Check if new isotope already exists :
      for(int i = 0; i < m_iNumber; i++){
          if( notation == list[i]->notation){
              qDebug("Isotope had been already initialized!");
                   qDebug("Energy: %f, linesNumber: %i", list[i]->getSpectralLine(0).energy,list[i]->spectralLinesNumber());
             return list[i];
          }
      }
    }

    temp = new Isotope* [m_iNumber];
    for(int i = 0; i < m_iNumber; i++){
        temp[i] = list[i];    // copy pointers to the temporary array
    }



    if(list != 0)
       delete [] list;          // delete old array
     list = new Isotope* [m_iNumber + 1];  // allocate new array (+1 size)





    for(int i = 0; i < m_iNumber; i++){
        list[i] = temp[i];    // copy old pointers to the new array
    }

    delete [] temp;    // delete temporary array




    // Initialization of new isotope (nisotope)
    switch (notation){
    /*____________________Cs137  INITIALIZATION_____________________*/
    case Isotope::Cs137:{

     // The data preparation for the new isotope.


        float singleMass = 137.0;       // mass of the single atom - [a.m.u.].
        float halflife = 30.1671;       // Halflife - [Years].

        int linesNumber = 1;            // The number of spectral lines.
     // Initialization of spectral lines
        Isotope::Line *lines = new Isotope::Line[linesNumber];
       /*_______1st line______________ */
        lines[0].energy = 661700.0;     // Energy - [eV].
        lines[0].quantaDecay = 0.8499;  // Quanta decay - [0.0 - 1.0 || i.e. 0.8499 = 84.99%].
       /*______________________________*/


     // Creation of the new isotope.
        list[m_iNumber] = new Isotope(halflife, singleMass, linesNumber,lines);
        delete [] lines;
        list[m_iNumber]->notation = Isotope::Cs137;
        break;
    }
//        /*____________________Na22  INITIALIZATION_______________________*/
    case Isotope::Na22 :{


        float singleMass = 22.0;
        float halflife = 2.66;

        int linesNumber = 2;
        Isotope::Line *lines = new Isotope::Line[linesNumber];

        lines[0].energy = 1274530.0;
        lines[0].quantaDecay = 0.99937;

        lines[1].energy = 511003.0;
        lines[1].quantaDecay = 1.79;




     // Creation of the new isotope.
          list[m_iNumber] = new Isotope(halflife, singleMass, linesNumber,lines);
          delete [] lines;
          list[m_iNumber]->notation = Isotope::Na22;
        break;
    }

   }

   return list[m_iNumber++];
}
