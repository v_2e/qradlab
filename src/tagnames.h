#ifndef TAG_H
#define TAG_H

//Tags and their attributes namespace
namespace TagNames {
    const QString TAG_ROOT = "scheme";

     const QString TAG_ABSORBERS   = "absorbers";
     const QString TAG_DETECTORS   = "detectors";
     const QString TAG_INSTRUMENTS = "instruments";
     const QString TAG_SOURCES     = "sources";

      const QString TAG_ABSORBER   = "absorber";
      const QString TAG_DETECTOR   = "detector";
      const QString TAG_INSTRUMENT = "instrument";
      const QString TAG_SOURCE     = "source";

    const QString ATTR_X = "x";
    const QString ATTR_Y = "y";
}

#endif // TAG_H
