#include "wiringboard.h"
#include <QPainter>
#include <connecter.h>
#include <QKeyEvent>
#include <QList>
#include <QtDebug>
#include "instrument.h"
#include <QSvgRenderer>

WiringBoard::WiringBoard(QWidget *parent) :
    QWidget(parent)
{
    setPalette(Qt::transparent);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    waiting = false;
    this->setMouseTracking(true);
}

void WiringBoard::paintEvent(QPaintEvent *paint)
{
   // qDebug("%d",connections.size());
    int x1,x2;
    int y1, y2;
    raise();
    QPainter p(this);
    p.setPen(QPen(Qt::darkCyan,5));
    if(!waiting){
          for(int i = 0; i < connections.size(); i++){
             if(!connections.isEmpty()){
                 Pair pair = connections.at(i);
                 y1 = pair.a->parentWidget()->y()+pair.a->y()+pair.a->height()/2;
                 y2 = pair.b->parentWidget()->y()+pair.b->y()+pair.b->height()/2;
                 if(pair.a->type() == Connecter::IN){
                   x1 = pair.a->parentWidget()->x()+pair.a->x()+pair.a->width()/2 - 7;
                   x2 =  pair.b->parentWidget()->x()+ pair.b->x()+pair.b->width()/2 + 9;
                 }else{
                   x1 = pair.a->parentWidget()->x()+pair.a->x()+pair.a->width()/2 + 9;
                   x2 =  pair.b->parentWidget()->x()+ pair.b->x()+pair.b->width()/2 - 7;
                 }
                 p.drawImage(x1 - forkHalfWidth,y1 - forkHalfHeight,forkIcon);
                 p.drawImage(x2 - forkHalfWidth,y2 - forkHalfHeight,forkIcon);
                 p.drawLine(x1,y1,x2 ,y2);
             }
          }
     }else{
       p.setBrush(QBrush(QColor(20,20,20,100)));

//       float new_x1 = connecterWaitingForConnect->parentWidget()->x()+
//               connecterWaitingForConnect->x()+
//               connecterWaitingForConnect->width()/2 ;
//       float new_y1 = connecterWaitingForConnect->parentWidget()->y()+
//               connecterWaitingForConnect->y()+
//               connecterWaitingForConnect->height()/2 ;


       float new_x2 =cursorPos.x() ;
       float new_y2 = cursorPos.y();


       float new_x1;
       float new_y1 = connecterWaitingForConnect->parentWidget()->y()+
               connecterWaitingForConnect->y()+
               connecterWaitingForConnect->height()/2;

       if(connecterWaitingForConnect->type() == Connecter::IN){
         new_x1 = connecterWaitingForConnect->parentWidget()->x()+
                  connecterWaitingForConnect->x()+
                  connecterWaitingForConnect->width()/2 - 7;
       }else{
         new_x1 = connecterWaitingForConnect->parentWidget()->x()+
                  connecterWaitingForConnect->x()+
                  connecterWaitingForConnect->width()/2 + 9;
       }







       p.drawLine(new_x1,new_y1,new_x2,new_y2);
//        for(int i = 0; i < connections.size(); i++){

//           if(!connections.isEmpty()){
//               Pair pair = connections.at(i);
//           //    qDebug("SIZE : %d", connections.size());
//               p.drawLine(pair.a->parentWidget()->x()+pair.a->x()+pair.a->width()/2,pair.a->parentWidget()->y()+pair.a->y()+pair.a->height()/2,
//                          pair.b->parentWidget()->x()+ pair.b->x()+pair.b->width()/2,pair.b->parentWidget()->y()+pair.b->y()+pair.b->height()/2);
//           }else{
//             //  qDebug("EMPTY LIST");
//           }
//        }

        for(int i = 0; i < connections.size(); i++){
           if(!connections.isEmpty()){
               Pair pair = connections.at(i);
               y1 = pair.a->parentWidget()->y()+pair.a->y()+pair.a->height()/2;
               y2 = pair.b->parentWidget()->y()+pair.b->y()+pair.b->height()/2;
               if(pair.a->type() == Connecter::IN){
                 x1 = pair.a->parentWidget()->x()+pair.a->x()+pair.a->width()/2 - 7;
                 x2 =  pair.b->parentWidget()->x()+ pair.b->x()+pair.b->width()/2 + 9;
               }else{
                 x1 = pair.a->parentWidget()->x()+pair.a->x()+pair.a->width()/2 + 9;
                 x2 =  pair.b->parentWidget()->x()+ pair.b->x()+pair.b->width()/2 - 7;
               }
               p.drawImage(x1 - forkHalfWidth,y1 - forkHalfHeight,forkIcon);
               p.drawImage(x2 - forkHalfWidth,y2 - forkHalfHeight,forkIcon);
               p.drawLine(x1,y1,x2 ,y2);
           }
        }





    }
}

void WiringBoard::keyPressEvent(QKeyEvent *key)
{
    if(key->key() == Qt::Key_Escape){
        setAttribute(Qt::WA_TransparentForMouseEvents);
        resume();
        update();
    }
}

void WiringBoard::mouseMoveEvent(QMouseEvent *moved)
{
    cursorX = moved->x();
    cursorY = moved->y();
}

void WiringBoard::connectUnits(Connecter *a, Connecter *b)
{

    int dublicate = 0;
    for(int i = 0; i < connections.size(); i++){
        if( (a == connections.at(i).a && b == connections.at(i).b) ||
            (b == connections.at(i).a && a == connections.at(i).b)){
            dublicate++;
            qDebug("Error: Dublicated connection !");
        }

    }

    if((a != b) && (a->type() != b->type() ) && !dublicate){


       // Getting a detector pointer to an instrument.
       if(a->type() == Connecter::IN){
          Instrument *instrument = qobject_cast<Instrument*>(a->parentWidget());
          Detector *detector = qobject_cast<Detector*>(b->parentWidget());
          instrument->setDetector(detector);
       }else{
           Instrument *instrument = qobject_cast<Instrument*>(b->parentWidget());
           Detector *detector = qobject_cast<Detector*>(a->parentWidget());
           instrument->setDetector(detector);
       }

      // If only one connection is available for each connecter;
      // a->cutConnections();
      //  b->cutConnections();
       Pair newPair;
        newPair.a = a;
        newPair.b = b;
       connections.append(newPair);
    }
}

void WiringBoard::setCursorPos(QPoint pos)
{
    cursorPos = pos;
    update();
}

QPoint WiringBoard::getCursorPos()
{
    return cursorPos;
}

void WiringBoard::setForkIcon(QString path, QSize iconSize)
{
    QSvgRenderer renderer(path);

    // Prepare a QImage with desired characteritisc
    QImage image(iconSize, QImage::Format_ARGB32);
    image.fill(80000000);  // partly transparent background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    forkIcon = image;
    forkHalfWidth = iconSize.width()/2;
    forkHalfHeight = iconSize.height()/2;

}

void WiringBoard::wait()
{
    waiting = true;
    //setAttribute(Qt::WA_TransparentForMouseEvents,false);
}

void WiringBoard::resume()
{
  //  setAttribute(Qt::WA_TransparentForMouseEvents,true);
    waiting = false;
}

void WiringBoard::disconnectUnits(Connecter *a, Connecter *b)
{
    Pair somePair;
     for(int i = 0; i < connections.size(); i++){
         somePair = connections.at(i);
         if(somePair.a == a && somePair.b == b){
            connections.removeAt(i);
            break;
         }
     }
}

void WiringBoard::connecterActivated()
{
    this->setFocus();

    Connecter *activatedConnecter = qobject_cast<Connecter*>(WiringBoard::sender());
    cursorPos = activatedConnecter->pos() + activatedConnecter->parentWidget()->pos();
    if(!waiting){

        wait();
        //newPair.a = activatedConnecter;
          connecterWaitingForConnect = activatedConnecter;

    }else{

        connectUnits(connecterWaitingForConnect,activatedConnecter);


        // Unit which send request to connect is correct?



//        if(activatedConnecter && (newPair.a != activatedConnecter)){
//          newPair.b = activatedConnecter;
//          connections.append(newPair);
//        }else{
//          qDebug("invalid request!");
//        }

        resume();

    }
    update();
}

void WiringBoard::connectionCut()
{
    Connecter *bad = qobject_cast<Connecter*>(WiringBoard::sender());
    Pair pair;

    int connectionIndex = 0;
    while(connectionIndex < connections.size()){
       pair = connections.at(connectionIndex);
       if(pair.a == bad || pair.b == bad){
           connections.removeAt(connectionIndex);
       }else{
           connectionIndex++;
       }

    }

}

