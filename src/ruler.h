#ifndef RULER_H
#define RULER_H

#include <QWidget>

class Ruler : public QWidget
{
    Q_OBJECT
public:
    explicit Ruler(QWidget *parent = 0);
    void paintEvent(QPaintEvent *paint);
    void mouseMoveEvent(QMouseEvent *moved);
    void resizeEvent(QResizeEvent *resized);
    void setHorizontal();
    void setVertical();
    void drawArrow();
    int cursorX,cursorY;
    int rulerSize,maxLenght;
    QImage marking,arrow;
    double arrowPos;

private:
    bool horizontal,vertical;

signals:

public slots:
    void mouseGrabb(int pos);

};

#endif // RULER_H
